using System;

namespace Momo.Common
{
    public abstract partial class BaseTrackingChangeObject
    {
        public string TrackingChangeType { get; set; }
    }

    [Serializable]
    public class BaseFoodyEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
    
    /// <summary>
    /// Base class for entities
    /// </summary>
    public abstract partial class BaseEntity
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public int Id { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as BaseEntity);
        }

        private static bool IsTransient(BaseEntity obj)
        {
            return obj != null && Equals(obj.Id, default(int));
        }

        protected Type GetUnproxiedType()
        {
            return GetType();
        }
        public virtual string ToCSVFormat(string fieldTermial)
        {
            return string.Empty;
        }
        public virtual bool Equals(BaseEntity other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!IsTransient(this) &&
                !IsTransient(other) &&
                Equals(Id, other.Id))
            {
                var otherType = other.GetUnproxiedType();
                var thisType = GetUnproxiedType();
                return thisType.IsAssignableFrom(otherType) ||
                        otherType.IsAssignableFrom(thisType);
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (Equals(Id, default(int)))
                return base.GetHashCode();
            return Id.GetHashCode();
        }

        public static bool operator ==(BaseEntity x, BaseEntity y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(BaseEntity x, BaseEntity y)
        {
            return !(x == y);
        }
    }

    /// <summary>
    /// Base class for model
    /// </summary>
    public abstract partial class BaseModel
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public int Id { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as BaseModel);
        }

        private static bool IsTransient(BaseModel obj)
        {
            return obj != null && Equals(obj.Id, default(int));
        }

        protected Type GetUnproxiedType()
        {
            return GetType();
        }
        public virtual string ToCSVFormat(string fieldTermial)
        {
            return string.Empty;
        }
        public virtual bool Equals(BaseModel other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!IsTransient(this) &&
                !IsTransient(other) &&
                Equals(Id, other.Id))
            {
                var otherType = other.GetUnproxiedType();
                var thisType = GetUnproxiedType();
                return thisType.IsAssignableFrom(otherType) ||
                        otherType.IsAssignableFrom(thisType);
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (Equals(Id, default(int)))
                return base.GetHashCode();
            return Id.GetHashCode();
        }

        public static bool operator ==(BaseModel x, BaseModel y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(BaseModel x, BaseModel y)
        {
            return !(x == y);
        }
    }
}
