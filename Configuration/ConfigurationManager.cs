﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Momo.Common.Configuration
{
    public static class ConfigurationManager
    {
        private static string _evtName;

        public static IConfigurationRoot AppSettings { get; set; }
        public static IConfigurationRoot ConnectionStrings { get; set; }

        static ConfigurationManager()
        {
            //var builder = new ConfigurationBuilder()
            //.SetBasePath(Directory.GetCurrentDirectory())
            //.AddJsonFile("appsettings.json");

            //AppSettings = builder.Build();


            //builder = new ConfigurationBuilder()
            //.SetBasePath(Directory.GetCurrentDirectory())
            //.AddJsonFile("connectionstrings.json");

            //ConnectionStrings = builder.Build();
        }
        public static void SetEnvironment(string envName)
        {
            _evtName = envName;

            var dirConfig = Path.Combine(Directory.GetCurrentDirectory(), "Configs");

            var builder = new ConfigurationBuilder()
            .SetBasePath(dirConfig);

            if (string.IsNullOrEmpty(_evtName))
                builder.AddJsonFile("appsettings.json", optional: true);
            else
                builder.AddJsonFile($"appsettings.{_evtName}.json", optional: true);

            AppSettings = builder.Build();


            builder = new ConfigurationBuilder()
                .SetBasePath(dirConfig);
            //.AddJsonFile($"connectionstrings.{_evtName}.json", optional: true)
            //.AddJsonFile("connectionstrings.json", optional: true);

            if (string.IsNullOrEmpty(_evtName))
                builder.AddJsonFile("connectionstrings.json", optional: true);
            else
                builder.AddJsonFile($"connectionstrings.{_evtName}.json", optional: true);

            ConnectionStrings = builder.Build();

        }
    }
}
