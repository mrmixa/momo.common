﻿using Momo.Common.ComponentModel;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace Momo.Common
{
    public static class HttpRequestExtensions
    {
        public static string Url(this HttpRequest request)
        {
            var location = new Uri($"{request.Scheme}://{request.Host}{request.Path}{request.QueryString}");

            return location.AbsoluteUri;
        }
        public static bool IsLocal(this HttpRequest req)
        {
            var connection = req.HttpContext.Connection;
            if (connection.RemoteIpAddress != null)
            {
                if (connection.LocalIpAddress != null)
                {
                    return connection.RemoteIpAddress.Equals(connection.LocalIpAddress);
                }
                else
                {
                    return IPAddress.IsLoopback(connection.RemoteIpAddress);
                }
            }

            // for in memory TestServer or when dealing with default connection info
            if (connection.RemoteIpAddress == null && connection.LocalIpAddress == null)
            {
                return true;
            }

            return false;
        }
    }
    /// <summary>
    ///     Represents a common helper
    /// </summary>
    public static class CommonHelper
    {
        private static readonly Regex RegexStripDiacritics = new Regex(@"\p{IsCombiningDiacriticalMarks}+", RegexOptions.Compiled);

        //private static readonly Regex RegRemoveDoubleWhiteSpace = new Regex(@"\s{2,}", RegexOptions.Compiled);
        private static readonly Regex RegSpecialCharactor = new Regex(@"[^\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}\p{Pc}\p{Lm}@]", RegexOptions.Compiled);

        /// <summary>
        ///     Ensures the subscriber email or throw.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        public static string EnsureSubscriberEmailOrThrow(string email)
        {
            string output = EnsureNotNull(email);
            output = output.Trim();
            output = EnsureMaximumLength(output, 255);

            if (!IsValidEmail(output))
            {
                throw new Exception("Email is not valid.");
            }

            return output;
        }

        /// <summary>
        ///     Verifies that a string is in valid e-mail format
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>true if the string is a valid e-mail address and false if it's not</returns>
        public static bool IsValidEmail(string email)
        {
            bool result;
            if (String.IsNullOrEmpty(email))
                return false;
            email = email.Trim();
            result = Regex.IsMatch(email,
                                   @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            return result;
        }

        /// <summary>
        ///     Verifies that a string is in valid e-mail format
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>true if the string is a valid e-mail address and false if it's not</returns>
        public static bool IsValidEmail2(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        ///     Generate random digit code
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns>Result string</returns>
        public static string GenerateRandomDigitCode(int length)
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < length; i++)
                str = String.Concat(str, random.Next(10).ToString(CultureInfo.InvariantCulture));
            return str;
        }

        /// <summary>
        ///     Returns an random interger number within a specified rage
        /// </summary>
        /// <param name="min">Minimum number</param>
        /// <param name="max">Maximum number</param>
        /// <returns>Result</returns>
        public static int GenerateRandomInteger(int min = 0, int max = 2147483647)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }
        public static string GetRandomCode(int totalChars = 8)
        {
            return new string(
                Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", totalChars)
                    .Select(s =>
                    {
                        var cryptoResult = new byte[4];
                        new RNGCryptoServiceProvider().GetBytes(cryptoResult);
                        return s[new Random(BitConverter.ToInt32(cryptoResult, 0)).Next(s.Length)];
                    })
                    .ToArray());
        }
        /// <summary>
        ///     Ensure that a string doesn't exceed maximum allowed length
        /// </summary>
        /// <param name="str">Input string</param>
        /// <param name="maxLength">Maximum length</param>
        /// <returns>Input string if its lengh is OK; otherwise, truncated input string</returns>
        public static string EnsureMaximumLength(string str, int maxLength, bool addSubfix = false)
        {
            if (String.IsNullOrEmpty(str))
                return str;

            if (str.Length > maxLength)
                return str.Substring(0, maxLength) + (addSubfix ? "..." : "");
            return str;
        }

        /// <summary>
        ///     Ensures that a string only contains numeric values
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Input string with only numeric values, empty string if input is null/empty</returns>
        public static string EnsureNumericOnly(string str)
        {
            if (String.IsNullOrEmpty(str))
                return string.Empty;

            var result = new StringBuilder();
            foreach (char c in str)
            {
                if (Char.IsDigit(c))
                    result.Append(c);
            }
            return result.ToString();
        }

        /// <summary>
        ///     Ensure that a string is not null
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Result</returns>
        public static string EnsureNotNull(string str)
        {
            if (str == null)
                return string.Empty;

            return str;
        }

        /// <summary>
        ///     Indicates whether the specified strings are null or empty strings
        /// </summary>
        /// <param name="stringsToValidate">Array of strings to validate</param>
        /// <returns>Boolean</returns>
        public static bool AreNullOrEmpty(params string[] stringsToValidate)
        {
            bool result = false;
            Array.ForEach(stringsToValidate, str => { if (string.IsNullOrEmpty(str)) result = true; });
            return result;
        }

        public static string Substring(string source, int count, bool addTheeDot = true)
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;
            if (source.Length <= count)
                return source;

            source = source.Substring(0, count);

            int lastDot = source.LastIndexOf('.');
            if (lastDot < 0)
                lastDot = source.IndexOf(',');

            int lastWhiteSpace = source.LastIndexOf(' ');
            int lastIndex = 0;
            if (lastWhiteSpace < lastDot)
                lastIndex = lastDot;

            if (lastIndex > 0)
            {
                source = source.Substring(0, lastIndex);
            }

            return source;

        }

        /// <summary>
        ///     Sets a property on an object to a valuae.
        /// </summary>
        /// <param name="instance">The object whose property to set.</param>
        /// <param name="propertyName">The name of the property to set.</param>
        /// <param name="value">The value to set the property to.</param>
        public static void SetProperty(object instance, string propertyName, object value)
        {
            if (instance == null) throw new ArgumentNullException("instance");
            if (propertyName == null) throw new ArgumentNullException("propertyName");

            Type instanceType = instance.GetType();
            PropertyInfo pi = instanceType.GetProperty(propertyName);
            if (pi == null)
                throw new Exception(string.Format("No property '{0}' found on the instance of type '{1}'.", propertyName,
                                       instanceType));
            if (!pi.CanWrite)
                throw new Exception(string.Format("The property '{0}' on the instance of type '{1}' does not have a setter.",
                                       propertyName, instanceType));
            if (value != null && !value.GetType().IsAssignableFrom(pi.PropertyType))
                value = To(value, pi.PropertyType);
            pi.SetValue(instance, value, new object[0]);
        }

        public static TypeConverter GetFoodyCustomTypeConverter(Type type)
        {

            if (type == typeof(List<int>))
                return new GenericListTypeConverter<int>();
            if (type == typeof(List<decimal>))
                return new GenericListTypeConverter<decimal>();
            if (type == typeof(List<string>))
                return new GenericListTypeConverter<string>();
            //if (type == typeof(ShippingOption))
            //    return new ShippingOptionTypeConverter();

            return TypeDescriptor.GetConverter(type);
        }

        /// <summary>
        ///     Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <returns>The converted value.</returns>
        public static object To(object value, Type destinationType)
        {
            return To(value, destinationType, CultureInfo.InvariantCulture);
        }

        /// <summary>
        ///     Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <param name="culture">Culture</param>
        /// <returns>The converted value.</returns>
        public static object To(object value, Type destinationType, CultureInfo culture)
        {
            if (value != null)
            {
                Type sourceType = value.GetType();

                TypeConverter destinationConverter = GetFoodyCustomTypeConverter(destinationType);
                TypeConverter sourceConverter = GetFoodyCustomTypeConverter(sourceType);
                if (destinationConverter != null && destinationConverter.CanConvertFrom(value.GetType()))
                    return destinationConverter.ConvertFrom(null, culture, value);
                if (sourceConverter != null && sourceConverter.CanConvertTo(destinationType))
                    return sourceConverter.ConvertTo(null, culture, value, destinationType);
                if (destinationType.IsEnum && value is int)
                    return Enum.ToObject(destinationType, (int)value);
                if (!destinationType.IsAssignableFrom(value.GetType()))
                    return Convert.ChangeType(value, destinationType, culture);
            }
            return value;
        }

        /// <summary>
        ///     Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <typeparam name="T">The type to convert the value to.</typeparam>
        /// <returns>The converted value.</returns>
        public static T To<T>(object value)
        {
            //return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
            return (T)To(value, typeof(T));
        }

        /// <summary>
        ///     Convert enum for front-end
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Converted string</returns>
        public static string ConvertEnum(string str)
        {
            string result = string.Empty;
            char[] letters = str.ToCharArray();
            foreach (char c in letters)
                if (c.ToString(CultureInfo.InvariantCulture) != c.ToString().ToLower())
                    result += " " + c.ToString();
                else
                    result += c.ToString();
            return result;
        }

        /// <summary>
        /// support foreach null List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original"></param>
        /// <returns></returns>
        public static IEnumerable<T> AsNotNull<T>(this IEnumerable<T> original)
        {
            return original ?? new T[0];
        }

        public static string StripDiacritics(string accented)
        {
            if (string.IsNullOrWhiteSpace(accented))
                return string.Empty;

            string strFormD = accented.Normalize(NormalizationForm.FormD);

            return RegexStripDiacritics.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        public static string RemoveAsciiControlChars(string content)
        {
            var result = Regex.Replace(content, @"[\u0008]", string.Empty);
            return result;
        }
        public static string RemoveAsciiOptionChars(string content)
        {
            var result = Regex.Replace(content, @"[\x00-\x08\x0B\x0C\x0E-\x1F]", string.Empty, RegexOptions.Compiled);//\x26
            return result;
        }
        public static string RemoveDoubleWhiteSpace(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;

            return RemoveDoubleWhiteChar(' ', source);
        }

        public static string RemoveDoubleWhiteChar(char c, string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;
            return source.Replace(c.ToString(), "ᵔᵕ").Replace("ᵕᵔ", "").Replace("ᵔᵕ", c.ToString()).Trim();
        }

        public static string RemoveSpecialCharactors(string source, string repl = " ")
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;

            return RegSpecialCharactor.Replace(source, repl).Trim();
        }

        public static string ToAscii(string source, bool removeDoubleWhiteSpace = true)
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;

            string result = StripDiacritics(source);
            if (removeDoubleWhiteSpace)
                return RemoveDoubleWhiteSpace(result);

            return result;
        }

        public static string GetUrlNamePart(string input, bool smootUrl = true, int maxlenght = 150)
        {
            if (string.IsNullOrWhiteSpace(input)) return input ?? "";
            input = input.Replace(".", "").Replace("-", "").Replace("”", "").Replace("“", "").Replace(",", "");
            input = ToAscii(input, true);
            var rs = new Regex("\\W").Replace(input, "-");
            if (!smootUrl)
                return rs;

            var turn = 0;//seft while
            while (turn < 100 && rs.IndexOf("--", StringComparison.Ordinal) != -1)
            {
                rs = rs.Replace("--", "-");
                turn++;
            }
            //if (rs.Length > 1 && rs.LastIndexOf("-", StringComparison.Ordinal) == rs.Length - 1)
            rs = rs.Trim('-');
            if (rs.Length > maxlenght)
            {
                rs = rs.Substring(0, maxlenght);
                rs = rs.TrimEnd('-');
            }

            return rs.ToLower();
        }
        public static List<int> StringToIntegerList(string input, string separator = ",")
        {
            var result = new List<int>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                result = input.Split(separator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                              .Select(x =>
                                  {
                                      int intVal = 0;
                                      int.TryParse(x.Trim(), out intVal);
                                      return intVal;
                                  })
                              .Where(x => x > 0)
                              .ToList();
            }
            return result;
        }
        /// <summary>
        /// YenDao: string to List lay tat ca gia tri
        /// </summary>
        /// <param name="input"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static List<int> StringToIntegerListAll(string input, string separator = ",")
        {
            var result = new List<int>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                result = input.Split(separator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                .Select(x =>
                                {
                                    return Convert.ToInt32(x);
                                })
                                .ToList();
            }
            return result;
        }
        public static List<string> StringToListStrAll(string input, string separator = ",")
        {
            var result = new List<string>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                result = input.Split(separator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                //.Select(x =>
                                //{
                                //    return Convert.ToInt32(x);
                                //})
                                .ToList();
            }
            return result;
        }

        public static T ParseJsonToObject<T>(string jsonString)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
        }

        public static string ParseObjectToJsonString(object obj)
        {
            //var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //return jsonSerializer.Serialize(obj);
            var result = JsonConvert.SerializeObject(obj, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            return result;
        }
        public static string ListToJson(this List<int> lst, string str = ",")
        {
            string r = "";
            if (lst != null)
            {
                for (int i = 0; i < lst.Count; i++)
                {
                    if (i != 0)
                        r += str;
                    r += lst[i];
                }
            }
            return r;
        }


        public static string GetIpAddress(HttpRequest request)
        {
            string result = string.Empty;
            try
            {
                result = request.HttpContext.Connection.RemoteIpAddress.ToString();
                string sIPAddress = request.Headers["X-Fowarded-For"];
                if (!string.IsNullOrEmpty(sIPAddress))
                {
                    string[] ips = sIPAddress.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string ipFromProxy = string.Empty;
                    if (ips.Length > 0)
                        ipFromProxy = ips.Where(x => x.Length > 3).LastOrDefault();
                    if (!string.IsNullOrWhiteSpace(ipFromProxy))
                        result = ipFromProxy;
                }
            }
            catch (Exception ex)
            {

            }


            return result;
        }


        public static string GetServerIpAddress()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName()); // `Dns.Resolve()` method is deprecated.
            IPAddress ipAddress = ipHostInfo.AddressList[0];

            return ipAddress.ToString();
        }
        public static string GetServerHostName()
        {
            return Dns.GetHostName(); // `Dns.Resolve()` method is deprecated.            
        }
        public static string GetClientHostName(HttpRequest request)
        {
            try
            {
                var computer_name = System.Net.Dns.GetHostEntry(request.HttpContext.Connection.RemoteIpAddress).HostName.Split(new[] { '.' });
                //var ecn = System.Environment.MachineName;
                return computer_name.FirstOrDefault();
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetServerIp()
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }
            return IP4Address;
        }

        public static bool IsNumberOnly(string phone)
        {
            bool result;
            if (String.IsNullOrEmpty(phone))
                return false;
            phone = phone.Trim();
            result = Regex.IsMatch(phone, @"^\d+$");
            return result;
        }

        public static string StripHTML(string htmlString)
        {
            if (!string.IsNullOrWhiteSpace(htmlString))
            {
                string pattern = @"<(.|\n)*?>";

                return Regex.Replace(htmlString, pattern, string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

        #region Encode & Decode


        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        #endregion

        #region Date Time

        // get list of month between 2 days
        public static IEnumerable<DateTime> MonthsBetween(DateTime d0, DateTime d1)
        {
            return Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                             .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m));
        }

        // get first sunday of week based on year and weekNum
        public static DateTime FirstDateOfWeek(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-4);
        }

        // get previous Monday / Sunday
        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static string FormatDateTimeAddDay(DateTime date, string format)
        {
            var lstWeekDay = new string[] { "Chủ nhật, ", "Thứ 2, ", "Thứ 3, ", "Thứ 4, ", "Thứ 5, ", "Thứ 6, ", "Thứ 7, " };

            var weekDayInx = (int)date.DayOfWeek;

            var datestr = lstWeekDay[weekDayInx] + date.ToString(format);
            //"yyyy-MM-dd HH:mm:ss \"GMT\"zzz"
            return datestr;
        }

        #endregion

        #region Xml


        public static T XmlNodeToModel<T>(XmlNode xmlNode, params string[] includePropeties)
            where T : class, new()
        {
            return (T)XmlNodeToModel(xmlNode, typeof(T), includePropeties);
        }

        public static object XmlNodeToModel(XmlNode xmlNode, Type type, params string[] includePropeties)
        {
            if (type.IsPrimitive || type == typeof(string) || type == typeof(DateTime))
            {
                if (xmlNode.Attributes.Count > 0)
                    return GetValue(xmlNode.Attributes[0].Value, type);
                return GetValue(xmlNode.InnerText, type);
            }

            var model = Activator.CreateInstance(type);

            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                if (property.CanWrite && (includePropeties.Length == 0 || includePropeties.Contains(property.Name)))
                {
                    if (xmlNode.Attributes[property.Name] != null)
                    {
                        var value = GetValue(xmlNode.Attributes[property.Name].Value, property.PropertyType);
                        if (value != null)
                        {
                            property.SetValue(model, value, null);
                        }
                    }
                    else
                    {
                        var childNode = xmlNode.SelectSingleNode(property.Name);
                        if (childNode != null)
                        {
                            if (property.PropertyType.IsGenericType && property.PropertyType.GetInterface("IList") != null)
                            {
                                if (childNode.ChildNodes.Count > 0)
                                {
                                    var value = (IList)Activator.CreateInstance(property.PropertyType);
                                    var itemType = property.PropertyType.GetGenericArguments()[0];
                                    foreach (XmlNode item in childNode.ChildNodes)
                                    {
                                        value.Add(XmlNodeToModel(item, itemType));
                                    }
                                    property.SetValue(model, value, null);
                                }
                            }
                            else if (childNode.ChildNodes.Count == 1)
                            {
                                var value = XmlNodeToModel(childNode.FirstChild, property.PropertyType);
                                if (value != null)
                                    property.SetValue(model, value, null);
                            }
                        }
                    }
                }
            }

            return model;
        }

        public static object GetValue(string value, Type type, params string[] properties)
        {
            if (type.IsGenericType && type.GetInterface("IList") != null)
            {
                try
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(value);
                    if (doc.FirstChild.ChildNodes.Count > 0)
                    {
                        var result = (IList)Activator.CreateInstance(type);
                        var itemType = type.GetGenericArguments()[0];
                        foreach (XmlNode item in doc.FirstChild.ChildNodes)
                        {
                            result.Add(XmlNodeToModel(item, itemType, properties));
                        }
                        return result;
                    }
                    else
                        return null;
                }
                catch
                {
#if DEBUG
                    throw;
#endif
#pragma warning disable CS0162 // Unreachable code detected
                    return null;
#pragma warning restore CS0162 // Unreachable code detected
                }
            }

            if (TypeDescriptor.GetConverter(type).CanConvertFrom(typeof(string)))
            {
                try
                {
                    if (type.FullName == typeof(bool).FullName)
                    {
                        if (value == "1" || string.Compare(value, "True", true) == 0)
                            return true;
                        else
                            return false;
                    }

                    return CommonHelper.To(value, type);
                }
                catch
                {
#if DEBUG
                    throw;
#endif
#pragma warning disable CS0162 // Unreachable code detected
                    return null;
#pragma warning restore CS0162 // Unreachable code detected
                }
            }

            if (type.IsClass)
            {
                var doc = new XmlDocument();
                doc.LoadXml(value);
                return XmlNodeToModel(doc.FirstChild, type, properties);
            }

            return null;
        }

        public static T GetValue<T>(string value, params string[] properties)
            where T : new()
        {
            var type = typeof(T);
            var result = GetValue(value, type, properties);

            if (result != null)
                return (T)result;

            return default(T);
        }

        #endregion

        #region Ultility

        public static string ConvertToUnsign(string unicodeString)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = unicodeString.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string NormalizeText(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return string.Empty;

            return input.Normalize(System.Text.NormalizationForm.FormC);
        }

        public static string FormatOutLink(string html, string siteDomain)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            var lstElement = htmlDoc.DocumentNode.SelectNodes("//a");

            if (lstElement != null && lstElement.Count > 0)
            {
                foreach (var item in lstElement)
                {
                    var attrTarget = item.Attributes["target"];

                    if (attrTarget != null)
                    {
                        attrTarget.Value = "_blank";
                    }
                    else
                    {
                        item.Attributes.Add("target", "_blank");
                    }

                    var attrClass = item.Attributes["class"];

                    if (attrClass != null)
                    {
                        attrClass.Value = "qa-out-link";
                    }
                    else
                    {
                        item.Attributes.Add("class", "qa-out-link");
                    }

                    var attrRel = item.Attributes["rel"];
                    var attrHref = item.Attributes["href"];

                    if (attrHref != null && !attrHref.Value.Contains(siteDomain))
                    {
                        if (attrRel != null)
                        {
                            attrRel.Value = "nofollow";
                        }
                        else
                        {
                            item.Attributes.Add("rel", "nofollow");
                        }
                    }
                }
            }

            return htmlDoc.DocumentNode.InnerHtml;
        }

        public static string ConvertArrIdToStr(int[] arrId)
        {
            if (arrId == null)
                return null;
            return string.Join(",", arrId);
        }

        public static int ToInt(this object input)
        {
            string value;
            if (input == null)
            {
                return 0;
            }

            value = Convert.ToString(input);

            if (!string.IsNullOrEmpty(value))
            {
                return Convert.ToInt32(input);
            }
            return 0;
        }

        public static double ToDouble(this object input)
        {
            string value;
            if (input == null)
            {
                return 0d;
            }

            value = Convert.ToString(input);

            if (!string.IsNullOrEmpty(value))
            {
                return Convert.ToDouble(input);
            }
            return 0d;
        }
        public static Decimal? ToDecimal(this object input)
        {
            if (input == null || input == DBNull.Value)
            {
                return null;
            }

            var value = Convert.ToString(input);

            if (!string.IsNullOrEmpty(value))
            {
                return Convert.ToDecimal(input);
            }
            return null;
        }
        public static int[] ConvertStrToArrId(string s, char sp)
        {
            Regex idReg = new System.Text.RegularExpressions.Regex(@"^\d+$", System.Text.RegularExpressions.RegexOptions.Compiled);
            if (string.IsNullOrEmpty(s))
                return new int[0];
            return s.Split(sp).Where(p => idReg.IsMatch(p)).Select(p => int.Parse(p)).ToArray();
        }

        public static string ConvertArrNameToStr(string[] arrName)
        {
            return string.Join("-", arrName);
        }

        public static IEnumerable<string> SplitByLength(this string str, int maxLength)
        {
            for (int index = 0; index < str.Length; index += maxLength)
            {
                yield return str.Substring(index, Math.Min(maxLength, str.Length - index));
            }
        }

        public static string GetFullHtmlFieldNameWithoutPrefix(string fullFieldName)
        {
            string result = string.Empty;

            int flag = fullFieldName.LastIndexOf(".");

            if (flag > 0)
            {
                int startIndex = flag + 1;
                result = fullFieldName.Substring(startIndex, fullFieldName.Length - startIndex);
            }
            else
            {
                result = fullFieldName;
            }

            return result;
        }

        public static string GetUtmCampaign(string utm_source, string utm_medium, string utm_campaign, string utm_term = null, string utm_content = null, string url = null)
        {
            string result = string.Empty;

            utm_source = CommonHelper.StripDiacritics(utm_source);
            utm_medium = CommonHelper.StripDiacritics(utm_medium);
            utm_campaign = CommonHelper.StripDiacritics(utm_campaign);
            utm_term = CommonHelper.StripDiacritics(utm_term);
            utm_content = CommonHelper.StripDiacritics(utm_content);

            result = "utm_source=" + utm_source
                + "&utm_medium=" + utm_medium
                + (!string.IsNullOrEmpty(utm_term) ? ("&utm_term=" + utm_term) : string.Empty)
                + (!string.IsNullOrEmpty(utm_content) ? ("&utm_content=" + utm_content) : string.Empty)
                + "&utm_campaign=" + utm_campaign
                + (!string.IsNullOrEmpty(url) ? ("&url=" + url) : string.Empty);

            return result.Replace(" ", "+");
        }

        public static bool IsNullable<T>(T t) { return false; }

        public static bool IsNullable<T>(T? t) where T : struct { return true; }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }


        public static List<List<T>> SplitList<T>(IEnumerable<T> values, int groupSize, int? maxCount = null)
        {
            List<List<T>> result = new List<List<T>>();
            // Quick and special scenario
            if (values.Count() <= groupSize)
            {
                result.Add(values.ToList());
            }
            else
            {
                List<T> valueList = values.ToList();
                int startIndex = 0;
                int count = valueList.Count;
                int elementCount = 0;

                while (startIndex < count && (!maxCount.HasValue || (maxCount.HasValue && startIndex < maxCount)))
                {
                    elementCount = (startIndex + groupSize > count) ? count - startIndex : groupSize;
                    result.Add(valueList.GetRange(startIndex, elementCount));
                    startIndex += elementCount;
                }
            }

            return result;
        }

        public static string FormatLazyLoadingForImage(string html)
        {
            if (string.IsNullOrWhiteSpace(html))
                return string.Empty;

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);
            var lstElement = htmlDoc.DocumentNode.SelectNodes("//img");

            if (lstElement != null && lstElement.Count > 0)
            {
                foreach (var item in lstElement)
                {
                    var cloneNode = item.Clone();
                    var attrSrc = item.Attributes["src"];

                    if (attrSrc != null)
                    {
                        item.SetAttributeValue("data-original", attrSrc.Value);
                        item.Attributes.Remove(attrSrc);
                    }

                    // <noscript> node for SEO
                    var noScriptNode = htmlDoc.CreateElement("noscript");
                    noScriptNode.ChildNodes.Add(cloneNode);
                    item.ParentNode.InsertAfter(noScriptNode, item);
                }
            }

            return htmlDoc.DocumentNode.InnerHtml;
        }
        public static string[] DetectImageUrls(string html)
        {
            if (string.IsNullOrWhiteSpace(html))
                return new string[0];

            var rs = new List<string>();

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);
            var lstElement = htmlDoc.DocumentNode.SelectNodes("//img");

            if (lstElement != null && lstElement.Count > 0)
            {
                foreach (var item in lstElement)
                {
                    var cloneNode = item.Clone();
                    var attrSrc = item.Attributes["src"];

                    if (attrSrc != null)
                    {
                        //item.SetAttributeValue("data-original", attrSrc.Value);
                        //item.Attributes.Remove(attrSrc);
                        rs.Add(attrSrc.Value);
                    }

                    // <noscript> node for SEO
                    //var noScriptNode = htmlDoc.CreateElement("noscript");
                    //noScriptNode.ChildNodes.Add(cloneNode);
                    //item.ParentNode.InsertAfter(noScriptNode, item);
                }
            }

            return rs.ToArray();
        }
        public static string ResizeImageFromHtml(string html, int maxWidth)
        {
            if (!string.IsNullOrEmpty(html))
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);
                var lstElement = htmlDoc.DocumentNode.SelectNodes("//img");

                if (lstElement != null && lstElement.Count > 0)
                {
                    foreach (var item in lstElement)
                    {
                        var style = item.Attributes["style"];
                        //if (style == null)
                        //{
                        if (maxWidth == 0)
                        {
                            item.SetAttributeValue("style", "width:100%;");
                        }
                        else
                        {
                            item.SetAttributeValue("style", string.Format("width:{0}px;", maxWidth));
                        }
                        var divNode = item.ParentNode;

                        while (divNode != null && divNode.OriginalName != "div")
                        {
                            divNode = divNode.ParentNode;
                        }
                        if (divNode != null)
                            divNode.SetAttributeValue("style", "text-align:center");

                        //}
                        //else
                        //{

                        //}
                    }
                }

                return htmlDoc.DocumentNode.InnerHtml;
            }
            return html;
        }
        public static string RemoveTagFromHtml(string html, string tagName)
        {
            if (!string.IsNullOrEmpty(html))
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);
                var lstElement = htmlDoc.DocumentNode.SelectNodes(string.Format("//{0}", tagName));

                if (lstElement != null && lstElement.Count > 0)
                {
                    foreach (var item in lstElement)
                    {
                        item.Remove();
                    }
                }

                return htmlDoc.DocumentNode.InnerHtml;
            }
            return html;
        }
        public static string RemoveTagEmptyDataFromHtml(string html, string tagName)
        {
            if (!string.IsNullOrEmpty(html))
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);
                var lstElement = htmlDoc.DocumentNode.SelectNodes(string.Format("//{0}", tagName));

                if (lstElement != null && lstElement.Count > 0)
                {
                    foreach (var item in lstElement)
                    {
                        if (string.IsNullOrWhiteSpace(item.InnerText) && string.IsNullOrWhiteSpace(item.InnerHtml))
                            item.Remove();
                    }
                }

                return htmlDoc.DocumentNode.InnerHtml;
            }
            return html;
        }
        public static string RemoveImageFromHtml(string html)
        {
            if (!string.IsNullOrEmpty(html))
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);
                var lstElement = htmlDoc.DocumentNode.SelectNodes("//img");

                if (lstElement != null && lstElement.Count > 0)
                {
                    foreach (var item in lstElement)
                    {
                        item.Remove();
                    }
                }

                return htmlDoc.DocumentNode.InnerHtml;
            }
            return html;
        }
        public static string RemoveParagraphFromHtml(string html)
        {
            if (!string.IsNullOrEmpty(html))
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(html);
                var lstElement = htmlDoc.DocumentNode.SelectNodes("//p");

                if (lstElement != null && lstElement.Count > 0)
                {
                    foreach (var item in lstElement)
                    {
                        if (string.IsNullOrWhiteSpace(item.InnerHtml))
                            item.Remove();
                    }
                }

                return htmlDoc.DocumentNode.InnerHtml;
            }
            return html;
        }

        public static string AddSpacesToSentence(string text, bool preserveAcronyms)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                    if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                        (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                         i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                        newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        public static string InsertSpaceAfterDot(string text)
        {
            // result = Regex.Replace(text, @"\.(?! |$|..|...)", ". ");
            // result = Regex.Replace(text, @"\?(?! |$|?|??)", "? ");

            int Dot = 1, LastDot = 0, DotSpace = 0;
            int newLine = 1;

            while (Dot > 0)
            {
                Dot = text.Substring(LastDot).IndexOf(".");
                newLine = text.Substring(LastDot).IndexOf("\n");
                if (Dot > 0)
                {
                    DotSpace = text.Substring(LastDot).IndexOf(". ");
                    if (Dot != DotSpace)
                        text = text.Substring(0, LastDot + Dot + 1) + " "
                               + text.Substring(LastDot + Dot + 1);

                    LastDot += Dot + 1;
                }
            }

            text = text.Replace(". . .", "...");

            return text;
        }

        public static string FirstLetterOfFirstWordInSenteceUpperCase(string text)
        {
            string[] arrStr = text.Split(new string[] { ". " }, StringSplitOptions.None);

            for (int i = 0; i < arrStr.Length; i++)
            {
                arrStr[i] = FirstLetterUpperCase(arrStr[i]);
            }

            text = string.Join(". ", arrStr);

            arrStr = text.Split(new string[] { "\n" }, StringSplitOptions.None);

            for (int i = 0; i < arrStr.Length; i++)
            {
                arrStr[i] = FirstLetterUpperCase(arrStr[i]);
            }

            text = string.Join("\n", arrStr);

            return text;
        }

        public static string TitleCaseFormat(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                return textInfo.ToTitleCase(text);
            }
            return text;
        }
        public static string UpperCaseFirstLetterInSentence(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                string[] sentences = text.Split(new string[] { "." }, StringSplitOptions.None);

                var fullSb = new StringBuilder();
                for (int i = 0; i < sentences.Length; i++)
                {
                    var sentence = sentences[i].ToLower();
                    sentence = FirstLetterUpperCase(sentence);
                    fullSb.Append(sentence);
                }

                return fullSb.ToString();
            }
            return text;
        }
        public static string ReFormatReview(string review)
        {
            review = review.Trim();
            // review = RemoveDoubleWhiteSpace(review);
            review = InsertSpaceAfterDot(review);
            review = FirstLetterOfFirstWordInSenteceUpperCase(review);

            return review;
        }

        public static string ReFormatSpecialDesc(string specialDesc, int? top = 3)
        {
            if (!string.IsNullOrEmpty(specialDesc))
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(specialDesc);
                var lstElement = htmlDoc.DocumentNode.SelectNodes("//li");

                if (lstElement != null && lstElement.Count > 0)
                {
                    for (int i = 0; i < lstElement.Count; i++)
                    {
                        var element = lstElement[i];

                        element.InnerHtml = CommonHelper.RemoveHtmlTags(element.InnerHtml);

                        if (top.HasValue)
                        {
                            if (i > top - 1)
                            {
                                element.Remove();
                            }
                        }
                    }
                }

                return htmlDoc.DocumentNode.InnerHtml;
            }
            else
            {
                return null;
            }
        }

        public static string GetFanpageFromUrl(string fanpageUrl)
        {
            if (!string.IsNullOrEmpty(fanpageUrl))
            {
                if (fanpageUrl.Contains("/"))
                {
                    string fanpageName = string.Empty;

                    string[] arrStr = fanpageUrl.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                    fanpageName = arrStr[arrStr.Length - 1];

                    return fanpageName;
                }
                else
                {
                    return fanpageUrl;
                }
            }
            else
            {
                return fanpageUrl;
            }
        }

        public static string ToCurrency<T>(object value, string format = "{0:#,#}")
        {
            string str = string.Empty;

            if (value != null
                && !string.IsNullOrEmpty(value.ToString()))
            {
                str = string.Format(format, (T)value);
            }

            if (!string.IsNullOrEmpty(str))
            {
                return str;
            }

            return "0";
        }

        public static string ToHexString(byte[] source)
        {
            var mStringBuilder = new StringBuilder();

            if (source != null)
            {
                mStringBuilder.Capacity = source.Length * 2;

                foreach (byte x in source)
                {
                    mStringBuilder.AppendFormat("{0:x2}", x);
                }
            }

            return mStringBuilder.ToString();
        }

        public static byte[] ParseFromHexString(string hexString)
        {
            if (!string.IsNullOrEmpty(hexString))
            {
                int iLen = hexString.Length;

                byte[] aResult = new byte[iLen / 2];

                for (int i = 0; i < iLen; i += 2)
                {
                    aResult[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
                }

                return aResult;
            }

            return new byte[0];
        }

        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

        #endregion

        #region Url



        public static string ReplaceUnSecureImageLink(HttpContext context, string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return content;
            }

            if (IsSslRequest(context))
            {
                return content.Replace("http://media.foody.vn", "https://media.foody.vn");
                //return content.Replace(Foody.Images.MediaUtls.Instance.MediaConfig.MediaRootUrl, Foody.Images.MediaUtls.Instance.MediaConfig.SslMediaRootUrl);
            }

            return content;
        }
        static string[] IgnoneSubdomains = { "local", "beta", "dev", "m" };


        public static string ToPublicUrl(HttpContext httpContext, Uri relativeUri, bool includePort = false)
        {
            var uriBuilder = new UriBuilder
            {
                Host = httpContext.Request.Host.Host,
                Path = "/",
                //Port = 80,
                Scheme = relativeUri.Scheme
            };
            if (includePort)
            {
                uriBuilder.Port = relativeUri.Port;
            }
            //if (httpContext.Request.IsLocal)
            //{
            //    uriBuilder.Port = httpContext.Request.Url.Port;
            //}
            var uri = new Uri(uriBuilder.Uri, relativeUri.PathAndQuery);
            return uri.AbsoluteUri;
        }
        #endregion

        public static string FirstLetterUpperCase(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return string.Empty;

            if (source.Length == 1)
                return source.ToUpper();

            return source.Substring(0, 1).ToUpper() + source.Substring(1);
        }

        public static string ReplaceSingleQuote(this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                return input.Replace("'", "''");
            }
            return string.Empty;
        }

        public static string EscapeRegExp(string str)
        {
            var reg = new System.Text.RegularExpressions.Regex(@"[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]");
            return reg.Replace(str, "\\$&");
        }

        public static DateTime RemoveMilliSecond(this DateTime d)
        {
            return d.AddMilliseconds(-d.Millisecond);
        }

        public static string RemoveHtmlTags(string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                string noHTML = Regex.Replace(input, @"<.*?>", string.Empty).Trim();//@"<[^\s.]*?>" use this will get errors on api, please check this again
                return noHTML;
            }
            return input;

        }


        public static string ReFormatAppHtmlContent(string content, int imgSize = 0)
        {
            if (!string.IsNullOrWhiteSpace(content))
            {
                content = RemoveTagEmptyDataFromHtml(content, "p");
                return string.Format("<!DOCTYPE html><meta name=\"viewport\" content=\"initial-scale=1.0\" /><div style=\"color:#444;font-family:arial,helvetica,sans-serif;\">{0}</div>", ResizeImageFromHtml(Regex.Replace(content, "times new roman,times,serif", "arial,helvetica,sans-serif", RegexOptions.IgnoreCase), imgSize));
            }
            else
                return string.Empty;
        }

        public static string ReFormatAppHtmlContentWithoutImage(string content)
        {
            if (!string.IsNullOrWhiteSpace(content))
            {
                var formatedHtml = RemoveImageFromHtml(Regex.Replace(content, "times new roman,times,serif", "arial,helvetica,sans-serif", RegexOptions.IgnoreCase));
                formatedHtml = RemoveParagraphFromHtml(formatedHtml);
                return string.Format("<meta name=\"viewport\" content=\"initial-scale=1.0\" /><div style=\"color:#444;font-family:arial,helvetica,sans-serif;\">{0}</div>", formatedHtml);
            }
            else
                return string.Empty;
        }

        ///<summary>
        /// Uploads a stream using a multipart/form-data POST.
        ///</summary>
        ///<param name="requestUri"></param>
        ///<param name="postData">A NameValueCollection containing form fields 
        /// to post with file data<span class="code-SummaryComment"></param>
        ///<param name="fileData">An open, positioned stream containing the file data</param>
        ///<param name="fileName">Optional, a name to assign to the file data.</param>
        ///<param name="fileContentType">Optional. 
        /// If omitted, registry is queried using<paramref name="fileName"/>. 
        /// If content type is not available from registry, 
        /// application/octet-stream will be submitted.<span class="code-SummaryComment"></param>
        ///<param name="fileFieldName">Optional, 
        /// a form field name to assign to the uploaded file data. 
        /// If omitted the value 'file' will be submitted.<span class="code-SummaryComment"></param>
        ///<param name="cookies">Optional, can pass null. Used to send and retrieve cookies. 
        /// Pass the same instance to subsequent calls to maintain state if required.<span class="code-SummaryComment"></param>
        ///<param name="headers">Optional, headers to be added to request.</param>
        ///<returns></returns>
        /// Reference: 
        /// http://tools.ietf.org/html/rfc1867
        /// http://tools.ietf.org/html/rfc2388
        /// http://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.2
        /// 
        public static WebResponse PostFile(Uri requestUri, NameValueCollection postData, Stream fileData, string fileName, string fileContentType, string fileFieldName, CookieContainer cookies, NameValueCollection headers)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(requestUri);


            fileContentType = string.IsNullOrEmpty(fileContentType) ? "application/octet-stream" : fileContentType;

            fileFieldName = string.IsNullOrEmpty(fileFieldName) ? "file" : fileFieldName;

            if (headers != null)
            {
                // set the headers
                foreach (string key in headers.AllKeys)
                {
                    string[] values = headers.GetValues(key);
                    if (values != null)
                        foreach (string value in values)
                        {
                            webrequest.Headers.Add(key, value);
                        }
                }
            }
            webrequest.Method = "POST";

            if (cookies != null)
            {
                webrequest.CookieContainer = cookies;
            }

            string boundary = "------" + DateTime.Now.Ticks.ToString("x", CultureInfo.InvariantCulture);

            webrequest.ContentType = "multipart/form-data; boundary=" + boundary;

            StringBuilder sbHeader = new StringBuilder();

            // add form fields, if any
            if (postData != null)
            {
                foreach (string key in postData.AllKeys)
                {
                    string[] values = postData.GetValues(key);
                    if (values != null)
                        foreach (string value in values)
                        {
                            sbHeader.AppendFormat("--{0}\r\n", boundary);
                            sbHeader.AppendFormat("Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}\r\n", key, value);
                        }
                }
            }

            if (fileData != null)
            {
                sbHeader.AppendFormat("--{0}\r\n", boundary);
                sbHeader.AppendFormat("Content-Disposition: form-data; name=\"{0}\"; {1}\r\n", fileFieldName, string.IsNullOrEmpty(fileName) ? "" : string.Format(CultureInfo.InvariantCulture, "filename=\"{0}\"", Path.GetFileName(fileName)));

                sbHeader.AppendFormat("Content-Type: {0}\r\n\r\n", fileContentType);
            }


            byte[] header = Encoding.UTF8.GetBytes(sbHeader.ToString());
            byte[] footer = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            long contentLength = header.Length + (fileData != null ?
                fileData.Length : 0) + footer.Length;

            webrequest.ContentLength = contentLength;

            using (Stream requestStream = webrequest.GetRequestStream())
            {
                requestStream.Write(header, 0, header.Length);


                if (fileData != null)
                {
                    // write the file data, if any
                    byte[] buffer = new Byte[checked((uint)Math.Min(4096,
                        (int)fileData.Length))];
                    int bytesRead;
                    while ((bytesRead = fileData.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        requestStream.Write(buffer, 0, bytesRead);
                    }
                }

                // write footer
                requestStream.Write(footer, 0, footer.Length);

                return webrequest.GetResponse();
            }
        }


        public static T GetPostFileResult<T>(Uri requestUri, NameValueCollection postData, Stream fileData, string fileName, string fileContentType, string fileFieldName, CookieContainer cookies, NameValueCollection headers)
        {
            var response = PostFile(requestUri, postData, fileData, fileName, fileContentType, fileFieldName, cookies, headers);

            var sr = new System.IO.StreamReader(response.GetResponseStream());
            var data = sr.ReadToEnd();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);

            //var responseStream = response.GetResponseStream();
            //byte[] responseBytes = new byte[response.ContentLength];

            //responseStream.Read(responseBytes, 0, (int)response.ContentLength);

            //string responseJson = Encoding.UTF8.GetString(responseBytes);

            //return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responseJson);

        }

        //public static Domain.Customers.GenderType StringToGender(string gender)
        //{
        //    switch ((gender ?? "f").ToLower())
        //    {
        //        case "m":
        //            return Domain.Customers.GenderType.Male;
        //        case "f":
        //        default:
        //            return Domain.Customers.GenderType.Female;
        //    }
        //}

        public static string CombineUrl(params string[] paths)
        {
            string rootPath = paths.FirstOrDefault(p => p.Contains("://"));
            if (string.IsNullOrWhiteSpace(rootPath))
                rootPath = paths.FirstOrDefault(p => p.StartsWith("//", StringComparison.Ordinal));

            string result = string.Join("/", paths.Where(p => !string.IsNullOrWhiteSpace(p) && p != rootPath));
            result = result.Replace("\\", "/");
            result = RemoveDoubleWhiteChar('/', result);
            if (!string.IsNullOrWhiteSpace(rootPath))
            {
                rootPath = rootPath.TrimEnd('/');
                result = string.Join("/", rootPath, result);
            }

            return result;

        }

        public static string CombineHostUrl(HttpContext httpContext, string url)
        {
            var uriBuilder = new UriBuilder
            {
                Host = httpContext.Request.Host.Host,
                Path = "/",
                //Port = 80,
                Scheme = httpContext.Request.Scheme
            };

            var uri = new Uri(uriBuilder.Uri, url);
            return uri.AbsoluteUri;

        }

        // Delete - edit review in 1h
        // 3600 = _settingService.GetSettingByKey("Review.PeriodCanEdit", 3600));
        public static bool CanEdit(DateTime createOn)
        {
            return (DateTime.Now - createOn).TotalSeconds <= 3600;
        }

        #region format string

        #region #-- Public Static Method --#

        public static string FormatString(string inputStr)
        {
            //string pattern
            string rxLower = @"\,|\:";
            string rxUpper = @"\.|\?";

            // Check for empty string.
            if (string.IsNullOrEmpty(inputStr))
            {
                return string.Empty;
            }

            char[] array = inputStr.ToCharArray();

            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }

            //Loop Lower Regular : (, :)
            Match mLower = Regex.Match(inputStr, rxLower);
            while (mLower.Success)
            {
                if (mLower.Index < inputStr.Length - 1)
                {
                    //Lower Char
                    if (inputStr[mLower.Index + 1] != ' ')
                    {
                        array[mLower.Index + 1] = char.ToLower(array[mLower.Index + 1]);
                    }
                    else
                    {
                        array[mLower.Index + 2] = char.ToLower(array[mLower.Index + 2]);
                    }
                }
                mLower = mLower.NextMatch();
            }
            //Loop Upper Regular : (. ?)
            Match mUpper = Regex.Match(inputStr, rxUpper);
            while (mUpper.Success)
            {
                if (mUpper.Index < inputStr.Length - 1)
                {

                    //Upper char
                    if (inputStr[mUpper.Index + 1] != ' ')
                    {
                        array[mUpper.Index + 1] = char.ToUpper(array[mUpper.Index + 1]);
                    }
                    else
                    {
                        array[mUpper.Index + 2] = char.ToUpper(array[mUpper.Index + 2]);
                    }

                }
                mUpper = mUpper.NextMatch();
            }



            //
            string outPuts = new string(array);

            outPuts = RemoveWhiteSpaces(outPuts);
            outPuts = RemoveSpecialCharacters(outPuts);
            outPuts = FormatDownLine(outPuts);
            outPuts = FormartUppper(outPuts);
            return outPuts;
        }

        public static string GetSqlInClause(string inClause)
        {
            string[] arrString = inClause.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < arrString.Length; i++)
            {
                string str = arrString[i];

                str.Replace("'", "''");
                str = string.Format("'{0}'", str);

                arrString[i] = str;
            }

            return string.Join(",", arrString);
        }

        #endregion

        #region #-- Private Static Method
        private static string RemoveWhiteSpaces(string inputStr)
        {
            string pInputStr = inputStr.Trim();

            while (pInputStr.IndexOf("  ") >= 0)
            {
                pInputStr = pInputStr.Replace("  ", " ");
            }
            return pInputStr;
        }
        private static string RemoveSpecialCharacters(string inputStr)
        {
            string pInputStr = inputStr.Trim();

            while (pInputStr.IndexOf(" ,") >= 0)
            {
                pInputStr = pInputStr.Replace(" ,", ",");
            }

            while (pInputStr.IndexOf("\n ") >= 0)
            {
                pInputStr = pInputStr.Replace("\n ", "\n");
            }
            return pInputStr;
        }
        private static string FormatDownLine(string inputStr)
        {
            string rxDownLine = @"\n";

            // Check for empty string.
            if (string.IsNullOrEmpty(inputStr))
            {
                return string.Empty;
            }

            char[] array = inputStr.ToCharArray();


            //Loop DownLine Regular : (\n)
            Match mDownLine = Regex.Match(inputStr, rxDownLine);
            while (mDownLine.Success)
            {
                if (mDownLine.Index < inputStr.Length - 1)
                {
                    array[mDownLine.Index + 1] = char.ToUpper(array[mDownLine.Index + 1]);
                }
                mDownLine = mDownLine.NextMatch();
            }

            string outPuts = new string(array);

            return outPuts;
        }

        private static string FormartUppper(string inputStr)
        {
            string pInputStr = inputStr.Trim();

            while (pInputStr.IndexOf(". ") <= 0)
            {
                pInputStr = pInputStr.Replace(".", ". ");
            }
            return pInputStr;
        }
        #endregion

        #endregion


        public static string StreamToString(Stream stream)
        {
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public static bool? IsOpening(string startTimeText, string endTimeText)
        {
            try
            {
                DateTime startTime = new DateTime();
                DateTime endTime = new DateTime();

                if (startTimeText.Contains("AM") || startTimeText.Contains("PM"))
                    startTime = DateTime.ParseExact(startTimeText, "h:mm tt", CultureInfo.InvariantCulture);//.TimeOfDay;
                else
                    //format 24h
                    startTime = DateTime.ParseExact(startTimeText, "HH:mm", CultureInfo.InvariantCulture);//.TimeOfDay;

                if (endTimeText.Contains("AM") || endTimeText.Contains("PM"))
                    endTime = DateTime.ParseExact(endTimeText, "h:mm tt", CultureInfo.InvariantCulture);//.TimeOfDay;
                else
                    //format 24h
                    endTime = DateTime.ParseExact(endTimeText, "HH:mm", CultureInfo.InvariantCulture);//.TimeOfDay;

                if (startTime > endTime)
                    endTime = endTime.AddDays(1);
                var currentTime = DateTime.Now;//.TimeOfDay;
                if (startTime == endTime || (currentTime >= startTime && currentTime <= endTime))
                    return true;
                return false;
            }
            catch
            {
                return null;
            }
        }

        static public bool IsAsciiKeyword(string input)
        {
            foreach (var item in input)
            {
                if (item < 32 || item > 126)
                    return false;
            }

            return true;
        }

        private const double Radio = 6378160; //in meter

        /// <summary>
        /// Convert degrees to Radians
        /// </summary>
        /// <param name="x">Degrees</param>
        /// <returns>The equivalent in radians</returns>
        private static double ToRadians(double x)
        {
            return x * Math.PI / 180;
        }

        /// <summary>
        /// Calculate the distance between two places.
        /// </summary>
        /// <param name="lon1"></param>
        /// <param name="lat1"></param>
        /// <param name="lon2"></param>
        /// <param name="lat2"></param>
        /// <returns></returns>
        public static double DistanceBetweenPlaces(double lat1, double lon1, double lat2, double lon2)
        {
            double dlon = ToRadians(lon2 - lon1);
            double dlat = ToRadians(lat2 - lat1);

            double a = (Math.Sin(dlat / 2) * Math.Sin(dlat / 2)) + Math.Cos(ToRadians(lat1)) * Math.Cos(ToRadians(lat2)) * (Math.Sin(dlon / 2) * Math.Sin(dlon / 2));
            double angle = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return angle * Radio;
        }

        #region create reandom code
        public static string RandomReadableString(int length)
        {
            return "123456789ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz".RandomCode(length);
        }

        public static string RandomCode(this string characterSet, int length)
        {
            var rng = new RNGCryptoServiceProvider();
            var random = new byte[length];
            rng.GetNonZeroBytes(random);

            var buffer = new char[length];
            var usableChars = characterSet.ToCharArray();
            var usableLength = usableChars.Length;

            for (int index = 0; index < length; index++)
            {
                buffer[index] = usableChars[random[index] % usableLength];
            }

            return new string(buffer);
        }
        #endregion

        public static bool IsStaticResource(HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            string path = request.Path;
            string extension = Path.GetExtension(path);

            if (string.IsNullOrWhiteSpace(extension))
            {
                if (path.StartsWith("/__utm"))
                    return true;
                return false;
            }

            switch (extension.ToLower())
            {
                case ".axd":
                case ".ashx":
                case ".bmp":
                case ".css":
                case ".gif":
                case ".htm":
                case ".html":
                case ".ico":
                case ".jpeg":
                case ".jpg":
                case ".js":
                case ".png":
                case ".rar":
                case ".zip":
                case ".woff":
                case ".mp3":
                case ".mp4":
                case ".mov":
                    return true;
            }

            return false;
        }

        public static string GetPattenKey(string key)
        {
            int idx = key.LastIndexOf(':');
            if (idx < 0)
                return key;
            return key.Substring(0, idx);
        }
        public static T CloneJson<T>(this T source)
        {
            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source));
        }
        public static string GetDisplayName(string firstName, string lastName, string username)
        {
            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            {
                return string.Format("{0} {1}", firstName, lastName);
            }
            else
                if (!string.IsNullOrEmpty(firstName))
            {
                return firstName;
            }
            else
                    if (!string.IsNullOrEmpty(lastName))
            {
                return lastName;
            }
            else
            {
                return username;
            }
        }
        #region enum
        public static string GetDisplayName(this Enum value)
        {
            var type = value.GetType();
            if (!type.IsEnum) return String.Format("Type '{0}' is not Enum", type);

            var members = type.GetMember(value.ToString());
            if (members.Length == 0) return String.Format("Member '{0}' not found in type '{1}'", value, type.Name);

            var member = members[0];
            var attributes = member.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute), false);
            if (attributes.Length == 0) return value.ToString();
            //throw new ArgumentException(String.Format("'{0}.{1}' doesn't have DisplayAttribute", type.Name, value));

            var attribute = (System.ComponentModel.DataAnnotations.DisplayAttribute)attributes[0];
            return attribute.GetName();
        }
        public static string GetDisplayDescription(this Enum value)
        {
            var type = value.GetType();
            if (!type.IsEnum) return String.Format("Type '{0}' is not Enum", type);

            var members = type.GetMember(value.ToString());
            if (members.Length == 0) return String.Format("Member '{0}' not found in type '{1}'", value, type.Name);

            var member = members[0];
            var attributes = member.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute), false);
            if (attributes.Length == 0) return value.ToString();
            //throw new ArgumentException(String.Format("'{0}.{1}' doesn't have DisplayAttribute", type.Name, value));

            var attribute = (System.ComponentModel.DataAnnotations.DisplayAttribute)attributes[0];
            return attribute.GetDescription();
        }

        #endregion
        public static string Right(this string value, int length)
        {
            if (value.Length >= length)
                return value.Substring(value.Length - length);
            return "";
        }

        #region Cookies
        public static void AddValueCookies(HttpContext context, object cookieValue, string cookieName, string splitWord, int limit = 10, bool insert = true, DateTime? expired = null)
        {
            var strCookieValue = cookieValue != null ? cookieValue.ToString().Trim() : null;

            var value = GetCookies(context.Request, cookieName);
            if (string.IsNullOrWhiteSpace(value))
                value = strCookieValue;
            else
            {
                var buff = value.Split(new[] { splitWord }, StringSplitOptions.RemoveEmptyEntries);
                if (!buff.Any(x => string.Compare(x, strCookieValue, StringComparison.OrdinalIgnoreCase) == 0))
                { // value = buff.Aggregate((current, next) => current + splitWord + next) + splitWord + strCookieValue;
                    value = strCookieValue;
                    var len = limit > buff.Length ? buff.Length : limit - 1;
                    for (int i = 0; i < len; i++)
                    {
                        value += splitWord + buff[i];
                    }

                }
            }

            SetCookies(context.Response, cookieName, value, expired);
        }
        public static void CreateCookies(HttpResponse response, string name, string value, DateTime? expire)
        {
            var cookieOption = new CookieOptions()
            {
                Expires = expire
            };
            response.Cookies.Delete(name);
            response.Cookies.Append(name, value, cookieOption);
        }

        public static void SetCookies(HttpResponse response, string name, string value, DateTime? expire = null, string domain = null)
        {
            var cookieOption = new CookieOptions()
            {
                Expires = expire,
                Domain = domain
            };

            response.Cookies.Delete(name);
            response.Cookies.Append(name, value, cookieOption);
        }
        public static string GetCookies(HttpRequest request, string name)
        {
            return request.Cookies[name];
        }
        public static void RemoveCookies(HttpResponse response, string name)
        {
            CookieOptions option = new CookieOptions
            {
                Expires = DateTime.Now.AddYears(-99)
            };
            response.Cookies.Append(name, "", option);

        }

        public static void UpdateCookies(HttpResponse response, string name, string newValue, DateTime? expire)
        {
            CookieOptions option = new CookieOptions
            {
                Expires = expire
            };
            response.Cookies.Delete(name);
            response.Cookies.Append(name, newValue, option);
        }

        public static string ColorToHexCode(Color color)
        {
            return string.Format("#{0:x2}{1:x2}{2:x2}", color.R, color.G, color.B);
        }


        public static bool IsSslRequest(HttpContext context)
        {
            if (context == null || context.Request == null)
                return false;

            if (string.Compare(context.Request.Headers["x-forwad-proto"].First() ?? "", "https", StringComparison.OrdinalIgnoreCase) == 0
                || string.Compare(context.Request.Headers["X-Forwarded-Proto"].First() ?? "", "https", StringComparison.OrdinalIgnoreCase) == 0)
                return true;


            return context.Request.IsHttps;
        }

        public static bool IsFacebookCrawler(HttpContext context)
        {
            if (context == null || context.Request == null || string.IsNullOrWhiteSpace(context.Request.Headers["User-Agent"].ToString()))
                return false;
            return context.Request.Headers["User-Agent"].ToString().StartsWith("facebookexternalhit") || context.Request.Headers["User-Agent"].ToString().StartsWith("Facebot");
        }

        public static string EnsureProto(string link, HttpContext context)
        {
            if (link == null)
                return string.Empty;

            string proto = CommonHelper.IsSslRequest(context) ? "https" : "http";
            if (link.StartsWith("//", StringComparison.Ordinal))
                link = string.Format("{0}:{1}", proto, link);
            else if (link.StartsWith("http", StringComparison.Ordinal))
            {
                if (CommonHelper.IsSslRequest(context))
                    link = link.Replace("http://", "https://");
                else
                    link = link.Replace("https://", "http://");
            }

            return link;
        }

        #endregion


        public static System.Security.Cryptography.RijndaelManaged GetRijndaelManaged(String secretKey)
        {
            var keyBytes = new byte[16];
            var secretKeyBytes = System.Text.Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new System.Security.Cryptography.RijndaelManaged
            {
                Mode = System.Security.Cryptography.CipherMode.CBC,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }


        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            //System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            string key = "Momoadmin!@#^%$";

            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            //Get your key from config file to open the lock!
            string key = "Momoadmin!@#^%$";

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static byte[] Encrypt(byte[] plainBytes, System.Security.Cryptography.RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        public static byte[] Decrypt(byte[] encryptedData, System.Security.Cryptography.RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }


        // Encrypts plaintext using AES 128bit key and a Chain Block Cipher and returns a base64 encoded string

        public static String Encrypt(String plainText, String key)
        {
            var plainBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key)));
        }


        public static String Decrypt(String encryptedText, String key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return System.Text.Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key)));
        }

        public static String createSToken(String siteSecret)
        {
            String sessionId = new Guid().ToString();
            String jsonToken = createJsonToken(sessionId);
            return Encrypt(jsonToken, siteSecret);
        }

        private static String createJsonToken(String sessionId)
        {
            var jobject = new
            {
                session_id = sessionId,
                ts_ms = DateTime.Now.Millisecond
            };

            return Newtonsoft.Json.JsonConvert.SerializeObject(jobject);
        }

        public static object GetValueDB(this object input)
        {
            if (input == null)
                return DBNull.Value;
            return input;
        }

        public static string ConvertToStr(List<int> ids)
        {
            if (ids != null && ids.Count > 0)
            {
                return string.Join(",", ids);
            }

            return string.Empty;
        }

        public static string FormatNumberK(long number)
        {
            if (number > 1000000)
                return ((double)number / 1000000).ToString("N1").Replace(",", ".") + "M";
            if (number > 1000)
                return ((double)number / 1000).ToString("N1").Replace(",", ".") + "K";

            return number.ToString("N0").Replace(",", ".");

        }
        public static string FormatNumber(Object obj)
        {
            return Convert.ToInt64(obj).ToString("N0").Replace(",", ".") + " đồng";
        }
        public static string FormatPoints(decimal points)
        {
            return points.ToString("#,##0.00", new CultureInfo("vi-VN"));//"#.##0,##",
        }

        public static string GetLatitude(HttpRequest request)
        {
            return GetCookies(request, "ulat");
        }
        public static string GetLongitude(HttpRequest request)
        {
            return GetCookies(request, "ulon");
        }


        public static string RemoveIllegalChar(string source)
        {
            if (!string.IsNullOrWhiteSpace(source))
            {
                if (source.Contains('"'))
                {
                    source = source.Replace('"', '\"');
                }

                if (source.Contains("'"))
                {
                    source = source.Replace("'", "\\'");
                }

                return source;
            }

            return string.Empty;
        }

        public static string GetPublicIP()
        {
            try
            {
                string result = string.Empty;
                string url = "http://checkip.dyndns.org";
                System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                using (System.Net.WebResponse resp = req.GetResponse())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream()))
                    {
                        string response = sr.ReadToEnd().Trim();
                        string[] a = response.Split(':');
                        string a2 = a[1].Substring(1);
                        string[] a3 = a2.Split('<');
                        result = a3[0];
                    }
                }
                return result;
            }
            catch
            {
                return "";
            }
        }
        #region Log Change
        /// <summary>
        /// Yen Dao: Log change data, if data change log ",field name: a->b"
        /// </summary>
        /// <param name="old"></param>
        /// <param name="newD"></param>
        /// <param name="nameField"></param>
        /// <returns></returns>
        public static string LogChange(decimal old, decimal newD, string nameField)
        {
            string r = "";
            if (old != newD)
            {
                return ", " + nameField + ":" + old + "->" + newD;
            }
            return r;
        }
        public static string LogChange(object old, object newD, string nameField)
        {
            string r = "";
            if ((old != null && !old.Equals(newD))
                || (old == null && old + "" != newD + ""))
            {
                return ", " + nameField + ":" + Newtonsoft.Json.JsonConvert.SerializeObject(old) + "->" + Newtonsoft.Json.JsonConvert.SerializeObject(newD);
            }
            return r;
        }

        public static string LogChangeDate(DateTime? a, DateTime? b, string nameField, string formatStr = "yyyy/MM/dd HH:mm:ss")
        {
            string r = "";
            if (a != null && b != null && !a.Equals(b))
            {
                return ", " + nameField + ":" + Convert.ToDateTime(a).ToString(formatStr) + "->" + Convert.ToDateTime(b).ToString(formatStr);
            }
            else if (a == null && b != null)
            {
                return ", " + nameField + ":" + "" + "->" + Convert.ToDateTime(b).ToString(formatStr);
            }
            else if (a != null && b == null)
            {
                return ", " + nameField + ":" + Convert.ToDateTime(a).ToString(formatStr) + "->" + "";
            }
            return r;
        }
        public static string LogChangeLst(IList lst1, IList lst2, string nameField)
        {
            string r = "";
            if (lst1 != null && lst2 != null
                && lst1.Count == lst2.Count)
            {
                int length = lst1.Count;
                for (int i = 0; i < length; i++)
                {
                    var type = lst1[i].GetType();
                    if (type.IsClass && type != typeof(string) &&
                        LogChange_Properties(lst1[i], lst2[i], nameField).Length > 0
                        )
                        return LogChange(lst1, lst2, nameField);
                    else if (!lst1[i].Equals(lst2[i]))
                    {
                        return LogChange(lst1, lst2, nameField);
                    }
                }
            }
            else
            {//lst1 == null && lst2 != null || lst1!=null && lst2==null || lst1 ==null && lst2==null
                return LogChange(lst1, lst2, nameField);
            }

            return r;
        }
        /// <summary>
        /// get string columns change from -> to
        /// </summary>
        /// <param name="detailOld"></param>
        /// <param name="detail"></param>
        /// <param name="ExceptionColumns">columns do not compare</param>
        /// <returns></returns>
        public static string LogChange_Properties<T>(this T val1, T val2, string exceptionProperties = "")
        {
            string r = string.Empty;
            string[] arrExcepCol = exceptionProperties.Split(',');
            PropertyInfo[] fi = val1.GetType().GetProperties();
            foreach (PropertyInfo propertyInfo in fi)
            {
                string namePro = propertyInfo.Name;
                if (arrExcepCol.Contains(propertyInfo.Name))
                    continue;
                if (propertyInfo.PropertyType.Name.ToLower() == "datetime")
                {
                    DateTime val1D = (DateTime)propertyInfo.GetValue(val1);
                    DateTime val2D = (DateTime)propertyInfo.GetValue(val2);
                    r += LogChangeDate(val1D, val2D, propertyInfo.Name);
                }
                else if (propertyInfo.PropertyType.Name.ToLower().IndexOf("list") == 0)
                {
                    var val1D = propertyInfo.GetValue(val1);
                    var val2D = propertyInfo.GetValue(val2);
                    r += LogChangeLst((IList)val1D, (IList)val2D, propertyInfo.Name);
                }
                else
                {
                    var val1D = propertyInfo.GetValue(val1);
                    var val2D = propertyInfo.GetValue(val2);
                    r += LogChange(val1D, val2D, propertyInfo.Name);
                }
            }
            return r;
        }
        #endregion
        #region get string sql
        public static string GetSqlFromDate(string colName, DateTime date)
        {
            string sql = " and  " + colName + "> cast('" + date.ToString("yyyy/MM/dd") + "' as date)"; ;
            return sql;
        }
        public static string GetSqlToDate(string colName, DateTime date)
        {
            return " and " + colName + "<CAST('" + date.AddDays(1).ToString("yyyy/MM/dd") + "' as date)";
        }
        #endregion

        public static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DataTable CreateDataTable_DBO_Notify_TableNowOrderRequestTable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("OrderId", typeof(Int32));
            datatable.Columns.Add("Status", typeof(Int32));
            datatable.Columns.Add("ReceiverId", typeof(Int32));
            datatable.Columns.Add("OrderCode", typeof(string));
            datatable.Columns.Add("OrderDate", typeof(DateTime));
            datatable.Columns.Add("RestaurantId", typeof(Int32));
            datatable.Columns.Add("AppType", typeof(Int32));
            datatable.Columns.Add("ReservationId", typeof(Int32));
            datatable.Columns.Add("MoreData", typeof(string));
            return datatable;
        }

        public static DataTable CreateDataTable_DBO_DeliveryOrderRequestTable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("OrderId", typeof(Int32));
            datatable.Columns.Add("ActionType", typeof(Int32));
            datatable.Columns.Add("ReceiverId", typeof(Int32));
            datatable.Columns.Add("ObjectType", typeof(Int32));
            datatable.Columns.Add("Status", typeof(Int32));
            datatable.Columns.Add("MoreData", typeof(string));
            datatable.Columns.Add("TotalOrders", typeof(Int32));
            datatable.Columns.Add("AppType", typeof(Int32));
            datatable.Columns.Add("resID", typeof(Int32));
            datatable.Columns.Add("VerifiedPhone", typeof(string));
            return datatable;
        }

        public static DataTable CreateDataTable_DBO_ECouponRequestTable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("EcouponId", typeof(Int32));
            datatable.Columns.Add("EcouponOrderId", typeof(Int32));
            datatable.Columns.Add("UserId", typeof(Int32));
            datatable.Columns.Add("EcouponName", typeof(string));
            datatable.Columns.Add("EndDate", typeof(DateTime));
            return datatable;
        }

        public static DataTable CreateDataTable_DBO_TopUpBonusRequestTable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("EventId", typeof(Int32));
            datatable.Columns.Add("PaymentMethod", typeof(Int32));
            datatable.Columns.Add("BonusAmount", typeof(double));
            datatable.Columns.Add("BonusPercent", typeof(Int32));
            datatable.Columns.Add("MaximumBonus", typeof(double));
            return datatable;
        }

        public static DataTable CreateDataTable_DBO_PayNow_UserMoneyRequestTable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("UserId", typeof(int));
            datatable.Columns.Add("Email", typeof(string));
            datatable.Columns.Add("Amount", typeof(double));
            datatable.Columns.Add("ReferenceId", typeof(string));
            datatable.Columns.Add("TransactionNumber", typeof(string));
            datatable.Columns.Add("Description", typeof(string));
            datatable.Columns.Add("ExcelRow", typeof(int));
            datatable.Columns.Add("RowNumber", typeof(int));
            return datatable;
        }

        public static DataTable CreateDataTable_DBO_Notify_PayNowImportRequestTable()
        {
            DataTable datatable = new DataTable();
            datatable.Columns.Add("UserId", typeof(Int32));
            datatable.Columns.Add("AppType", typeof(Int32));
            datatable.Columns.Add("ObjectId", typeof(Int32));
            datatable.Columns.Add("ActionType", typeof(Int32));
            datatable.Columns.Add("ObjectType", typeof(Int32));
            datatable.Columns.Add("MoreData", typeof(string));
            datatable.Columns.Add("Message", typeof(string));
            return datatable;
        }

        //public static string EncodeNonAsciiCharacters(string value)
        //{
        //    // Get byte of UTF-32
        //    Byte[] bytes = Encoding.UTF32.GetBytes(value);
        //    StringBuilder asAscii = new StringBuilder();
        //    for (int idx = 0; idx < bytes.Length; idx += 4)
        //    {
        //        uint codepoint = BitConverter.ToUInt32(bytes, idx);
        //        if (codepoint <= 127)
        //            asAscii.Append(Convert.ToChar(codepoint));
        //        else
        //            //asAscii.AppendFormat("\\u{0:x4}", codepoint);
        //            asAscii.AppendFormat("&#{0};", codepoint);
        //    }
        //    asAscii.Append(";");
        //    return asAscii.ToString();
        //}

        public static string DecodeNonAsciiCharacters(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return value;
            value = Regex.Replace(value, @"amp;", string.Empty);
            MatchCollection matchList = Regex.Matches(value, @"&#[0-9]+(;)");
            string[] listString = matchList.Cast<Match>().Select(match => match.Value).ToArray();
            // Regex.Split(value, @"&#|;");
            // Delete whitespace in array string
            listString = listString.Where(x => !string.IsNullOrEmpty(x)).ToArray();

            //StringBuilder sb = new StringBuilder();
            foreach (string s in listString)
            {
                var match = Regex.Match(s, @"\d+");
                var number = match.Value;
                if (!string.IsNullOrEmpty(number))
                {
                    int output;
                    bool result = int.TryParse(number, out output);
                    if (result)
                    {
                        // Convert the string to unsigned int.
                        uint tempValue = Convert.ToUInt32(number);
                        //sb.Append(Char.ConvertFromUtf32((int)tempValue) + " ");
                        //sb.Append(Char.ConvertFromUtf32((int)tempValue));
                        var newChar = Char.ConvertFromUtf32((int)tempValue);
                        value = value.Replace(s, newChar);
                    }
                    //else
                    //{
                    //    sb.Append(s + " ");
                    //    sb.Append(s);
                    //}
                }
            }

            //return sb.ToString();
            return HttpUtility.HtmlDecode(value);
        }

        public static string ObjectToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static T JsonToObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string SwapContent(this string value, string swapValue)
        {
            if (string.IsNullOrWhiteSpace(value))
                return swapValue;

            return value;
        }

        public static string GetPrettyDate(DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "vừa đăng";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 phút trước";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} phút trước",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 giờ trước";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} giờ trước",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "hôm qua";
            }
            return d.ToString("dd/MM/yyyy");
        }


        public static string SantinizerString(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return value;
            value = Regex.Replace(value, @"<[^>]*>", String.Empty);

            value = value.Replace("{{", "");
            value = value.Replace("}}", "");
            value = value.Replace("constructor(", "");

            return value;
        }

        public static object EnumToArray(Type enumType)
        {
            var names = Enum.GetNames(enumType);
            var values = Enum.GetValues(enumType);

            var rs = new List<object>();

            for (int i = 0; i < names.Length; i++)
            {
                rs.Add(new { Name = names[i], Value = Convert.ToInt32(values.GetValue(i)) });
            }

            return rs;
        }


        public static List<DateTime> DateStringToDateTime(string dateString,string parseFormat = "yyyy/MM/dd")
        {
            var result = new List<DateTime>();
            try
            {
                var dates = dateString.Replace(" ", "").Split('-').ToList();

                result.Add(DateTime.ParseExact(dates[0], parseFormat, CultureInfo.InvariantCulture));
                result.Add(DateTime.ParseExact(dates[1], parseFormat, CultureInfo.InvariantCulture).AddHours(23).AddMinutes(59));
                return result;
            }
            catch
            {
                return result;
            }
        
           
        }

        public static TAttribute GetAttribute<TAttribute>(Enum enumValue)
    where TAttribute : Attribute
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<TAttribute>();
        }
    }
}