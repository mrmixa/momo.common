﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Momo.Common.Helpers
{
    public class SecurityBaseHelper
    {
        public static string CreateSaltKey(int size)
        {
            // Generate a cryptographic random number
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number
            return Convert.ToBase64String(buff);
        }
        public static string CreatePasswordHash(string password, string saltkey, string passwordFormat = "SHA1")
        {
            var saltAndPassword = String.Concat(password, saltkey);
            string hashedPassword  = SHA1HashStringForUTF8String(saltAndPassword);
            return hashedPassword;
        }

        /// <summary>
        /// Compute hash for string encoded as UTF8
        /// </summary>
        /// <param name="s">String to be hashed</param>
        /// <returns>40-character hex string</returns>
        public static string SHA1HashStringForUTF8String(string value)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(value);

            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);

            return HexStringFromBytes(hashBytes);
        }

        /// <summary>
        /// Convert an array of bytes to a string of hex digits
        /// </summary>
        /// <param name="bytes">array of bytes</param>
        /// <returns>String of hex digits</returns>
        public static string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }

        public static string GenerateRequestHash(string requestHashString, string secretKey)
        {
            //var osType = APICommonHelper.GetRequestClientOSType();
            var encoding = new System.Text.UTF8Encoding();
            //var secretKey = _settingService.GetSettingByKey<string>(string.Format("API.Sercurity.{0}.{1}.SecretKey", host, osType.ToString()), "123456");
            byte[] keyByte = encoding.GetBytes(secretKey);
            byte[] messageBytes = encoding.GetBytes(requestHashString);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        /// <summary>
        /// ham encrypt dung thuat toan AES256 (256 bits = 32 bytes)
        /// </summary>
        /// <param name="code"></param>
        /// <param name="encryptkey"></param>
        /// <param name="iVector"></param>
        /// <returns></returns>
        public static string EncryptCodeAES256(string code, string encryptkey, string iVector)
        {

            if (string.IsNullOrWhiteSpace(code))
            {
                throw new ArgumentNullException("Code");
            }
            CryptLib _crypt = new CryptLib();
            string key = CryptLib.getHashSha256(encryptkey, 32);
            return _crypt.encrypt(code, key, iVector);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="code"></param>
        /// <param name="encryptkey"></param>
        /// <param name="iVector"></param>
        /// <returns></returns>
        public static string DecryptCodeAES256(string code, string encryptkey, string iVector)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new ArgumentNullException("Code");
            }
            CryptLib _crypt = new CryptLib();
            string key = CryptLib.getHashSha256(encryptkey, 32);
            return _crypt.decrypt(code, key, iVector);
        }

        public static string EncodeBase64(string value)
        {
            byte[] concatenated = System.Text.ASCIIEncoding.ASCII.GetBytes(value);
            return System.Convert.ToBase64String(concatenated);
        }

        static readonly char[] padding = { '=' };
        public static string EncryptBase64(string clearText, string EncryptionKey)
        {
            try
            {
                //string EncryptionKey = "JHK@!$RTYEWQRGH";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray()).TrimEnd(padding).Replace('+', '-').Replace('/', '_');
                    }
                }
                return clearText;
            }
            catch
            {

            }
            return "";
        }

        public static string DecryptBase64(string cipherText, string EncryptionKey)
        {
            string incoming = cipherText.Replace('_', '/').Replace('-', '+');
            switch (cipherText.Length % 4)
            {
                case 2: incoming += "=="; break;
                case 3: incoming += "="; break;
            }
            try
            {
                //string EncryptionKey = "JHK@!$RTYEWQRGH";
                byte[] cipherBytes = Convert.FromBase64String(incoming);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return cipherText;
            }
            catch
            {

            }
            return "";
        }
    }
}
