﻿using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;

namespace Momo.Common.Routes
{
    public static class RouteWraper
    {
        public static IRouteBuilder RouteBuilder;
        public static void Init(IRouteBuilder routeBuilder)
        {
            RouteBuilder = routeBuilder;
            config.Routes = new RouteConfigCollection();
        }
        static RouteWraper()
        {
        }
        public static RoutingConfigs config = new RoutingConfigs();

        public static void MapRoute(string name, string url, object defaults, string[] namespaces)
        {
            var route = new RouteConfig();
            route.Name = name;
            route.Urls = new UrlConfigCollection();
            var urlC = new UrlConfig { };
            urlC.Pattern = url;
            route.Urls.Add(urlC);

            foreach (var item in namespaces)
            {
                route.Namespaces.Add(new NameConfig { Name = item });
            }

            var defaultsD = CreateRouteValueDictionaryUncached(defaults);

            foreach (var key in defaultsD.Keys)
            {
                var def = new NameValueConfig() { Name = key, Value = defaultsD[key].ToString() };
                route.RoutingDefaults.Add(def);
            }

            config.Routes.Add(route);

            RouteBuilder.MapRoute(name, url, defaults, namespaces);
        }

        public static string GetRouteXml()
        {

            string xmlConfig = "";
            foreach (RouteConfig item in config.Routes)
            {
                string url = "";
                var urlE = item.Urls.GetEnumerator();
                if (urlE.MoveNext())
                    url = (urlE.Current as UrlConfig).Pattern;
                string defaults = "";
                string constraints = "";
                string namespaces = "";
                foreach (var ns in item.Namespaces)
                {
                    namespaces += string.Format("<add Name=\"{0}\"></add>\n", ns);
                }
                foreach (NameValueConfig def in item.RoutingDefaults)
                {
                    defaults += string.Format("<add Name=\"{0}\" Value=\"{1}\"></add>\n", def.Name, def.Value);
                }
                foreach (NameValueConfig con in item.RoutingConstraints)
                {
                    constraints += string.Format("<add Name=\"{0}\" Value=\"{1}\"></add>\n", con.Name, con.Value);
                }

                xmlConfig += @"<add Name=""" + item.Name + @""">
                  <Urls>
                    <add Pattern=""" + url + @""" RoutingSchema=""Default""></add>
                  </Urls>
                  <RoutingDefaults>
                   " + defaults + @"
                  </RoutingDefaults>
                   <RoutingConstraints>
                   " + constraints + @"
                  </RoutingConstraints>
                  <Namespaces>
                    " + namespaces + @"
                  </Namespaces>
                </add>" + "\n";
            }

            return xmlConfig;
        }

        public static void MapRoute(string name, string url, object defaults, object constraints, string[] namespaces)
        {
            var route = new RouteConfig();
            route.Name = name;
            route.Urls = new UrlConfigCollection();
            var urlC = new UrlConfig { };
            urlC.Pattern = url;
            route.Urls.Add(urlC);

            foreach (var item in namespaces)
            {
                route.Namespaces.Add(new NameConfig { Name = item });
            }

            var defaultsD = CreateRouteValueDictionaryUncached(defaults);

            foreach (var key in defaultsD.Keys)
            {
                var def = new NameValueConfig() { Name = key, Value = defaultsD[key].ToString() };
                route.RoutingDefaults.Add(def);
            }
            var constraintsD = CreateRouteValueDictionaryUncached(constraints);

            foreach (var key in constraintsD.Keys)
            {
                var def = new NameValueConfig() { Name = key, Value = constraintsD[key].ToString() };
                route.RoutingConstraints.Add(def);
            }

            config.Routes.Add(route);

            RouteBuilder.MapRoute(name, url, defaults, namespaces);
        }

        private static RouteValueDictionary CreateRouteValueDictionaryUncached(object values)
        {
            var dictionary = values as IDictionary<string, object>;
            if (dictionary != null)
            {
                return new RouteValueDictionary(dictionary);
            }

            return ObjectToDictionaryUncached(values);
        }

        public static RouteValueDictionary ObjectToDictionaryUncached(object value)
        {
            RouteValueDictionary dictionary = new RouteValueDictionary();

            if (value != null)
            {
                foreach (PropertyHelper helper in PropertyHelper.GetProperties(value))
                {
                    dictionary.Add(helper.Name, helper.GetValue(value));
                }
            }

            return dictionary;
        }


    }

    internal class PropertyHelper
    {
        private static ConcurrentDictionary<Type, PropertyHelper[]> _reflectionCache = new ConcurrentDictionary<Type, PropertyHelper[]>();

        private Func<object, object> _valueGetter;

        /// <summary>
        /// Initializes a fast property helper. This constructor does not cache the helper.
        /// </summary>
        public PropertyHelper(PropertyInfo property)
        {
            Contract.Assert(property != null);

            Name = property.Name;
            _valueGetter = MakeFastPropertyGetter(property);
        }

        /// <summary>
        /// Creates a single fast property setter. The result is not cached.
        /// </summary>
        /// <param name="propertyInfo">propertyInfo to extract the getter for.</param>
        /// <returns>a fast setter.</returns>
        /// <remarks>This method is more memory efficient than a dynamically compiled lambda, and about the same speed.</remarks>
        public static Action<TDeclaringType, object> MakeFastPropertySetter<TDeclaringType>(PropertyInfo propertyInfo)
            where TDeclaringType : class
        {
            Contract.Assert(propertyInfo != null);

            MethodInfo setMethod = propertyInfo.GetSetMethod();

            Contract.Assert(setMethod != null);
            Contract.Assert(!setMethod.IsStatic);
            Contract.Assert(setMethod.GetParameters().Length == 1);
            Contract.Assert(!propertyInfo.ReflectedType.IsValueType);

            // Instance methods in the CLR can be turned into static methods where the first parameter
            // is open over "this". This parameter is always passed by reference, so we have a code
            // path for value types and a code path for reference types.
            Type typeInput = propertyInfo.ReflectedType;
            Type typeValue = setMethod.GetParameters()[0].ParameterType;

            Delegate callPropertySetterDelegate;

            // Create a delegate TValue -> "TDeclaringType.Property"
            var propertySetterAsAction = setMethod.CreateDelegate(typeof(Action<,>).MakeGenericType(typeInput, typeValue));
            var callPropertySetterClosedGenericMethod = _callPropertySetterOpenGenericMethod.MakeGenericMethod(typeInput, typeValue);
            callPropertySetterDelegate = Delegate.CreateDelegate(typeof(Action<TDeclaringType, object>), propertySetterAsAction, callPropertySetterClosedGenericMethod);

            return (Action<TDeclaringType, object>)callPropertySetterDelegate;
        }

        public virtual string Name { get; protected set; }

        public object GetValue(object instance)
        {
            Contract.Assert(_valueGetter != null, "Must call Initialize before using this object");

            return _valueGetter(instance);
        }

        /// <summary>
        /// Creates and caches fast property helpers that expose getters for every public get property on the underlying type.
        /// </summary>
        /// <param name="instance">the instance to extract property accessors for.</param>
        /// <returns>a cached array of all public property getters from the underlying type of this instance.</returns>
        public static PropertyHelper[] GetProperties(object instance)
        {
            return GetProperties(instance, CreateInstance, _reflectionCache);
        }

        /// <summary>
        /// Creates a single fast property getter. The result is not cached.
        /// </summary>
        /// <param name="propertyInfo">propertyInfo to extract the getter for.</param>
        /// <returns>a fast getter.</returns>
        /// <remarks>This method is more memory efficient than a dynamically compiled lambda, and about the same speed.</remarks>
        public static Func<object, object> MakeFastPropertyGetter(PropertyInfo propertyInfo)
        {
            Contract.Assert(propertyInfo != null);

            MethodInfo getMethod = propertyInfo.GetGetMethod();
            Contract.Assert(getMethod != null);
            Contract.Assert(!getMethod.IsStatic);
            Contract.Assert(getMethod.GetParameters().Length == 0);

            // Instance methods in the CLR can be turned into static methods where the first parameter
            // is open over "this". This parameter is always passed by reference, so we have a code
            // path for value types and a code path for reference types.
            Type typeInput = getMethod.ReflectedType;
            Type typeOutput = getMethod.ReturnType;

            Delegate callPropertyGetterDelegate;
            if (typeInput.IsValueType)
            {
                // Create a delegate (ref TDeclaringType) -> TValue
                Delegate propertyGetterAsFunc = getMethod.CreateDelegate(typeof(ByRefFunc<,>).MakeGenericType(typeInput, typeOutput));
                MethodInfo callPropertyGetterClosedGenericMethod = _callPropertyGetterByReferenceOpenGenericMethod.MakeGenericMethod(typeInput, typeOutput);
                callPropertyGetterDelegate = Delegate.CreateDelegate(typeof(Func<object, object>), propertyGetterAsFunc, callPropertyGetterClosedGenericMethod);
            }
            else
            {
                // Create a delegate TDeclaringType -> TValue
                Delegate propertyGetterAsFunc = getMethod.CreateDelegate(typeof(Func<,>).MakeGenericType(typeInput, typeOutput));
                MethodInfo callPropertyGetterClosedGenericMethod = _callPropertyGetterOpenGenericMethod.MakeGenericMethod(typeInput, typeOutput);
                callPropertyGetterDelegate = Delegate.CreateDelegate(typeof(Func<object, object>), propertyGetterAsFunc, callPropertyGetterClosedGenericMethod);
            }

            return (Func<object, object>)callPropertyGetterDelegate;
        }

        private static PropertyHelper CreateInstance(PropertyInfo property)
        {
            return new PropertyHelper(property);
        }

        // Implementation of the fast getter.
        private delegate TValue ByRefFunc<TDeclaringType, TValue>(ref TDeclaringType arg);

        private static readonly MethodInfo _callPropertyGetterOpenGenericMethod = typeof(PropertyHelper).GetMethod("CallPropertyGetter", BindingFlags.NonPublic | BindingFlags.Static);
        private static readonly MethodInfo _callPropertyGetterByReferenceOpenGenericMethod = typeof(PropertyHelper).GetMethod("CallPropertyGetterByReference", BindingFlags.NonPublic | BindingFlags.Static);

        private static object CallPropertyGetter<TDeclaringType, TValue>(Func<TDeclaringType, TValue> getter, object @this)
        {
            return getter((TDeclaringType)@this);
        }

        private static object CallPropertyGetterByReference<TDeclaringType, TValue>(ByRefFunc<TDeclaringType, TValue> getter, object @this)
        {
            TDeclaringType unboxed = (TDeclaringType)@this;
            return getter(ref unboxed);
        }

        // Implementation of the fast setter.
        private static readonly MethodInfo _callPropertySetterOpenGenericMethod = typeof(PropertyHelper).GetMethod("CallPropertySetter", BindingFlags.NonPublic | BindingFlags.Static);

        private static void CallPropertySetter<TDeclaringType, TValue>(Action<TDeclaringType, TValue> setter, object @this, object value)
        {
            setter((TDeclaringType)@this, (TValue)value);
        }

        protected static PropertyHelper[] GetProperties(object instance,
                                                        Func<PropertyInfo, PropertyHelper> createPropertyHelper,
                                                        ConcurrentDictionary<Type, PropertyHelper[]> cache)
        {
            // Using an array rather than IEnumerable, as this will be called on the hot path numerous times.
            PropertyHelper[] helpers;

            Type type = instance.GetType();

            if (!cache.TryGetValue(type, out helpers))
            {
                // We avoid loading indexed properties using the where statement.
                // Indexed properties are not useful (or valid) for grabbing properties off an anonymous object.
                IEnumerable<PropertyInfo> properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                                           .Where(prop => prop.GetIndexParameters().Length == 0 &&
                                                                          prop.GetMethod != null);

                var newHelpers = new List<PropertyHelper>();

                foreach (PropertyInfo property in properties)
                {
                    PropertyHelper propertyHelper = createPropertyHelper(property);

                    newHelpers.Add(propertyHelper);
                }

                helpers = newHelpers.ToArray();
                cache.TryAdd(type, helpers);
            }

            return helpers;
        }
    }
}
