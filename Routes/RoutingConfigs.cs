﻿using Momo.Common.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Momo.Common.Routes
{
    public enum RouteSchema
    {
        Default = 0,
        Vietnam = 86
    }
    public static class RoutingConfigUtls
    {
        public static RoutingConfigs GetRoutingConfig()
        {
            return RoutingConfigs;
        }
        public static readonly RoutingConfigs RoutingConfigs;

        public static RouteSchema CurrentRouteSchema { get; private set; }

        static RoutingSchemaConfig _currentSchemaConfig;
        public static RoutingSchemaConfig CurrentSchemaConfig
        {
            get
            {
                if (_currentSchemaConfig == null)
                    _currentSchemaConfig = GetRoutingSchemaConfig(CurrentRouteSchema);
                return _currentSchemaConfig;
            }
        }


        static RoutingConfigUtls()
        {
            RoutingConfigs = RoutingConfigs.Load();
            CurrentRouteSchema = RoutingConfigs.RoutingSchemas.DefaultSchema;

            string schemaConfig = ConfigurationManager.AppSettings["RouteSchema"];
            if (string.IsNullOrWhiteSpace(schemaConfig) == false)
            {
                RouteSchema temp;
                if (Enum.TryParse(schemaConfig, out temp))
                    CurrentRouteSchema = temp;
            }
        }

        

        static string GetDefaultProvinceConstraint(int countryId)
        {
            switch (countryId)
            {
                default:
                    return "_LOCATION_";
            }
        }

        static string GetProvinceRouteContraint()
        {
            //try
            //{
            //    var dbValue = DB.DbHelper.ExecuteScalar("select cast((select '|'+UrlRewriteName from StateProvince where Published=1for xml path('')) as varchar(max))");
            //    if (dbValue != null && dbValue != DBNull.Value)
            //    {
            //        string constraint = dbValue.ToString();
            //        if (string.IsNullOrWhiteSpace(constraint) == false)
            //            return GetDefaultProvinceConstraint() + "|" + constraint.Trim('|', ' ');
            //    }
            //}
            //catch
            //{

            //}

            return "";
        }

        static string GetCategoryRouteContraint()
        {
            //try
            //{
            //    var dbValue = DB.DbHelper.ExecuteScalar("select cast((select '|'+UrlRewriteName from res_category where IsActive=1 for xml path('')) as varchar(max))");
            //    if (dbValue != null && dbValue != DBNull.Value)
            //    {
            //        string constraint = dbValue.ToString();
            //        if (string.IsNullOrWhiteSpace(constraint) == false)
            //            return GetDefaultCategoryConstraint() + "|" + constraint.Trim('|', ' ');
            //    }
            //}
            //catch
            //{

            //}

            return "";
        }

        static string GetCategoryGroupRouteContraint()
        {
            //try
            //{
            //    var dbValue = DB.DbHelper.ExecuteScalar("select '|'+GroupKey from res_categoryGroup where IsActive=1 for xml path('')");
            //    if (dbValue != null && dbValue != DBNull.Value)
            //    {
            //        string constraint = dbValue.ToString();
            //        if (string.IsNullOrWhiteSpace(constraint) == false)
            //            return constraint.Trim('|', ' ');
            //    }
            //}
            //catch
            //{

            //}

            return "";
        }

        static string GetArticleCategoryRouteContraint()
        {
            //try
            //{
            //    var dbValue = DB.DbHelper.ExecuteScalar("SELECT '|'+UrlRewriteName FROM ArticleCategory where IsActive=1 for xml path('')");
            //    if (dbValue != null && dbValue != DBNull.Value)
            //    {
            //        string constraint = dbValue.ToString();
            //        if (string.IsNullOrWhiteSpace(constraint) == false)
            //            return constraint;//.Trim('|', ' ');
            //    }
            //}
            //catch
            //{

            //}

            return "";
        }

        static string GetPropertyRouteContraint()
        {
            //try
            //{
            //    var dbValue = DB.DbHelper.ExecuteScalar("select '|'+UrlRewriteName from Res_RestaurantProperty p WHERE p.UrlRewriteName IS NOT NULL order by p.UrlRewriteName for xml path('')");
            //    if (dbValue != null && dbValue != DBNull.Value)
            //    {
            //        string constraint = dbValue.ToString();
            //        if (!string.IsNullOrWhiteSpace(constraint))
            //            constraint = string.Format("({0})(,(\\w*|\\d*-\\w*|\\d*)*)*", constraint.TrimStart('|'));
            //        return constraint;
            //    }
            //}
            //catch
            //{

            //}

            return "";
        }

        public static void MapRoutes(IRouteBuilder routes, RouteSchema? schema = null)
        {
            RoutingSchemaConfig routingSchemas = null;
            if (schema == null)
                schema = CurrentRouteSchema;

            if (CurrentRouteSchema == schema.Value)
                routingSchemas = CurrentSchemaConfig;
            else
                routingSchemas = GetRoutingSchemaConfig(schema.Value);

          
            foreach (RouteConfig item in RoutingConfigs.Routes)
            {
                if (!item.Disabled)
                {
                    var dataTokens = new RouteValueDictionary();
                    dataTokens.Add("RouteName", item.Name);
                    

                    if ((item.Namespaces != null) && (item.Namespaces.Count > 0))
                    {
                        List<string> namespaces = new List<string>(item.Namespaces.Count);
                        foreach (NameConfig config in item.Namespaces)
                        {
                            namespaces.Add(config.Name);
                        }
                        dataTokens["Namespaces"] = namespaces.ToArray();
                    }

                    routes.MapRoute(
                        item.Name,
                        GetUrl(item, schema.Value, routingSchemas),
                        CreateRouteValueDictionary(item.RoutingDefaults, routingSchemas),
                        CreateRouteValueDictionary(item.RoutingConstraints, routingSchemas),
                        dataTokens
                        );
                }
            }
        }

        private static RoutingSchemaConfig GetRoutingSchemaConfig(RouteSchema? schema)
        {
            RoutingSchemaConfig routingSchemas = null;
            foreach (RoutingSchemaConfig item in RoutingConfigs.RoutingSchemas)
            {
                if (RoutingConfigs.RoutingSchemas.Count == 1)
                    routingSchemas = item;
                else
                {
                    if (routingSchemas == null)
                        routingSchemas = item;
                    if (item.Name == schema)
                    {
                        routingSchemas = item;
                        break;
                    }
                }
            }

            var provinceConstraint = GetProvinceRouteContraint();
            var categoryConstraint = GetCategoryRouteContraint();
            var propertyConstraint = GetPropertyRouteContraint();
            var categoryGroupConstraint = GetCategoryGroupRouteContraint();
            var articleCategoryConstraint = GetArticleCategoryRouteContraint();
            if (string.IsNullOrWhiteSpace(provinceConstraint) == false)
                routingSchemas.LocationsRouting = provinceConstraint;
            if (string.IsNullOrWhiteSpace(categoryConstraint) == false)
                routingSchemas.CategoriesRouting = categoryConstraint;

            if (string.IsNullOrWhiteSpace(propertyConstraint) == false)
                routingSchemas.PropertyConstraint = propertyConstraint;

            if (string.IsNullOrWhiteSpace(categoryGroupConstraint) == false)
                routingSchemas.CategoryGroupRouting = categoryGroupConstraint;

            if (string.IsNullOrWhiteSpace(articleCategoryConstraint) == false)
                routingSchemas.ArticleCategoryRouting = articleCategoryConstraint;

            return routingSchemas;
        }

        static string GetUrl(RouteConfig routeConfig, RouteSchema schema, RoutingSchemaConfig schemaConfig)
        {
            string url = null;
            List<UrlConfig> urls = new List<UrlConfig>(routeConfig.Urls.Count);
            foreach (UrlConfig item in routeConfig.Urls)
            {
                urls.Add(item);
            }
            var urlc = urls.FirstOrDefault(x => x.RoutingSchema == schema);
            if (urlc == null)
                urlc = urls.FirstOrDefault(x => x.RoutingSchema == RouteSchema.Default);
            if (urlc == null)
                urlc = urls.FirstOrDefault();

            if (urlc == null)
            {
                throw new ArgumentNullException("url");
            }

            url = urlc.Pattern;

            return url;
        }        

        private static RouteValueDictionary CreateRouteValueDictionary(NameValueConfigCollection values, RoutingSchemaConfig schemaConfig)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (NameValueConfig item in values)
            {
                object value = item.Value;
                switch (item.Value.ToLower())
                {
                    case "{{optional}}":
                        value = "UrlParameter.Optional";
                        break;
                    case "{{locationroutingdefault}}":
                        value = schemaConfig.LocationRoutingDefault;
                        break;
                    case "{{categoriesrouting}}":
                        value = schemaConfig.CategoriesRouting;
                        break;

                    case "{{allcategoryrouting}}":
                        value = schemaConfig.AllCategoryRouting;
                        break;
                    case "{{locationsrouting}}":
                        value = schemaConfig.LocationsRouting;
                        break;
                    case "{{listaction}}":
                        value = schemaConfig.ListAction;
                        break;
                    case "{{albumname}}":
                        value = schemaConfig.AlbumName;
                        break;
                    case "{{propertyconstraint}}":
                        value = schemaConfig.PropertyConstraint;
                        break;
                    case "{{categorygrouprouting}}":
                        value = schemaConfig.CategoryGroupRouting;
                        break;
                    case "{{categorygrouproutingdefault}}":
                        value = schemaConfig.CategoryGroupRoutingDefault;
                        break;
                    case "{{articlecategoryrouting}}":
                        value = schemaConfig.ArticleCategoryRouting;
                        break;
                }

                dictionary.Add(item.Name, value);
            }

            return new RouteValueDictionary(dictionary);
        }



        public static string GetMicrositeAction(string action)
        {
            switch ((action ?? "").ToLower())
            {
                case "gioi-thieu":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "intro";
                    break;
                case "binh-luan":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "review";
                    break;
                case "album-anh":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "album-photo";
                    break;
                case "photo":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "album-photo";
                    break;
                case "thuc-don":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "menu";
                    break;
                case "khuyen-mai":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "promotion";
                    break;
                case "album-khong-gian":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "space-ablum";
                    break;
                case "album-mon-an":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "dish-album";
                    break;
                case "album-ca-nhan":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "personal-album";
                    break;
                case "album-khac":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "other-album";
                    break;
                case "quang-cao":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "pr";
                    break;
                case "hoi-dap":
                    if (RoutingConfigUtls.CurrentRouteSchema == RouteSchema.Default)
                        action = "qa";
                    break;

            }

            return action;

        }
    }
    public class RoutingConfigs
    {
        public static RoutingConfigs Load()
        {
            return new RoutingConfigs();
        }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<?xml version=\"1.0\"?>");
            result.AppendLine("<RoutingConfigs>");
            result.AppendLine(RoutingSchemas.ToString());
            result.AppendLine(Routes.ToString());
            result.Append("</RoutingConfigs>");
            return result.ToString();
        }

        public RoutingSchemaConfigCollection RoutingSchemas
        {
            get; set;
        }

        public RouteConfigCollection Routes
        {
            get; set;
        }
    }

    public class RoutingSchemaConfigCollection : List<RoutingSchemaConfig>
    {
        public RouteSchema DefaultSchema
        {
            get; set;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<RoutingSchemas DefaultSchema=\"Vietnam\">");
            foreach (RoutingSchemaConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</RoutingSchemas>");
            return result.ToString();
        }
    }
    public class RoutingSchemaConfig
    {
        public override string ToString()
        {
            return @"<add Name=""" + Name + @"""
               LocationRoutingDefault=""" + LocationRoutingDefault + @"""
               AllCategoryRouting=""" + AllCategoryRouting + @"""
               CategoriesRouting=""" + CategoriesRouting + @"""
               LocationsRouting=""" + LocationsRouting + @"""
               ListAction=""" + ListAction + @"""
               CategoryGroupRoutingDefault=""" + CategoryGroupRoutingDefault + @"""
               CategoryGroupRouting=""" + CategoryGroupRouting + @"""
               ArticleCategoryRouting=""" + ArticleCategoryRouting + @"""
               AlbumName=""" + AlbumName + @"""
         >
    </add>";
        }

        public RouteSchema Name
        {
            get; set;
        }

        public string LocationRoutingDefault
        {
            get;
        }

        public string CategoriesRouting
        {
            get; set;
        }


        public string AllCategoryRouting
        {
            get; set;
        }


        public string LocationsRouting
        {
            get; set;
        }


        public string PropertyConstraint
        {
            get; set;
        }

        public string CategoryGroupRoutingDefault
        {
            get; set;
        }


        public string CategoryGroupRouting
        {
            get; set;
        }


        public string ArticleCategoryRouting
        {
            get; set;
        }

        public string ListAction
        {
            get; set;
        }


        public string AlbumName
        {
            get; set;
        }
    }

    public class RouteConfigCollection : List<RouteConfig>
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<Routes>");
            foreach (RouteConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</Routes>");
            return result.ToString();
        }
    }

    public class TestUrlConfigCollection : List<TestUrlConfig>
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<TestUrls>");
            foreach (TestUrlConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</TestUrls>");
            return result.ToString();
        }
    }

    public class TestUrlConfig
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<add Name=\"" + Name + "\" Schema=\"" + Schema + "\" Profile=\"" + Profile + "\" Url=\"" + Url.Replace("&", "&amp;") + "\"");

            if (!string.IsNullOrWhiteSpace(RequestMethod) && RequestMethod != "GET")
                result.Append(" RequestMethod=\"" + RequestMethod + "\"");
            if (!string.IsNullOrWhiteSpace(RequestHeaders))
                result.Append(" RequestHeaders=\"" + RequestHeaders + "\"");
            if (!string.IsNullOrWhiteSpace(RequestParams))
                result.Append(" RequestParams=\"" + RequestParams + "\"");
            if (!string.IsNullOrWhiteSpace(RequestBody))
                result.Append(" RequestHeaders=\"" + RequestBody + "\"");
            if (ExpectedHttpStatusCode != 200)
                result.Append(" ExpectedHttpStatusCode=\"" + ExpectedHttpStatusCode + "\"");
            if (!string.IsNullOrWhiteSpace(ExpectedResponseUrl))
                result.Append(" ExpectedResponseUrl=\"" + ExpectedResponseUrl + "\"");
            if (!string.IsNullOrWhiteSpace(ExpectedResponseText))
                result.Append(" ExpectedResponseText=\"" + ExpectedResponseText + "\"");
            if (ExpectedResponseTimeGoalInMiniSeconds > 0)
                result.Append(" ExpectedResponseTimeGoalInMiniSeconds=\"" + ExpectedResponseTimeGoalInMiniSeconds + "\"");
            if (!string.IsNullOrWhiteSpace(ExpectedResponseRequiredTag))
                result.Append(" ExpectedResponseRequiredTag=\"" + ExpectedResponseRequiredTag + "\"");
            if (!string.IsNullOrWhiteSpace(ExpectedResponseCookie))
                result.Append(" ExpectedResponseCookie=\"" + ExpectedResponseCookie + "\"");
            if (!string.IsNullOrWhiteSpace(ExpectedResponseHeader))
                result.Append(" ExpectedResponseHeader=\"" + ExpectedResponseHeader + "\"");
            if (RequiredAuth)
                result.Append(" RequiredAuth=\"" + RequiredAuth + "\"");

            result.Append("/>");

            return result.ToString();
        }

        public string Name
        {
            get; set;
        }

        public string Profile
        {
            get; set;
        }

        public string Platform
        {
            get; set;
        }

        public string Schema
        {
            get; set;
        }

        public string Url
        {
            get; set;
        }

        public string RequestCookies
        {
            get; set;
        }

        public bool RequiredAuth
        {
            get; set;
        }

        public string RequestMethod
        {
            get; set;
        }

        public string RequestHeaders
        {
            get; set;
        }

        public string RequestParams
        {
            get; set;
        }

        public string RequestBody
        {
            get; set;
        }

        public int ExpectedHttpStatusCode
        {
            get; set;
        }

        public string ExpectedResponseUrl
        {
            get; set;
        }

        public string ExpectedResponseText
        {
            get; set;
        }

        public int ExpectedResponseTimeGoalInMiniSeconds
        {
            get; set;
        }

        public string ExpectedResponseRequiredTag
        {
            get; set;
        }

        public string ExpectedResponseCookie
        {
            get; set;
        }

        public string ExpectedResponseHeader
        {
            get; set;
        }
    }

    public class RouteConfig
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append("<add Name=\"" + Name + "\"");
            if (Disabled)
                result.Append(" Disabled=\"" + Disabled + "\"");
            if (Order != 100)
                result.Append(" Order=\"" + Order + "\"");
            result.AppendLine(">");

            result.AppendLine(Urls.ToString());
            result.AppendLine(RoutingDefaults.ToString("RoutingDefaults"));
            result.AppendLine(RoutingConstraints.ToString("RoutingConstraints"));
            result.AppendLine(Namespaces.ToString());
            if (TestUrls.Count > 0)
                result.AppendLine(TestUrls.ToString());
            else
            {
                var testUrls = new TestUrlConfigCollection();
                int count = 0;
                foreach (UrlConfig item in Urls)
                {
                    bool requiredAuth = item.Pattern.StartsWith("tai-khoan") || item.Pattern.StartsWith("account");
                    if (!item.Pattern.Contains("{"))
                        testUrls.Add(new TestUrlConfig
                        {
                            Name = "test" + count++,
                            Schema = item.RoutingSchema.ToString(),
                            Profile = "Live",
                            Url = item.Pattern.Replace("&", "&amp;"),
                            RequiredAuth = requiredAuth
                        });
                    else
                    {
                        testUrls.Add(new TestUrlConfig
                        {
                            Name = "test" + count++,
                            Schema = item.RoutingSchema.ToString(),
                            Profile = "Live",
                            Url = GetTestUrlWithDynamicRouteValue(item),
                            RequiredAuth = requiredAuth
                        });
                    }
                }
                if (testUrls.Count > 0)
                    result.AppendLine(testUrls.ToString());
            }
            result.Append("</add>");

            return result.ToString();
        }

        private string GetTestUrlWithDynamicRouteValue(UrlConfig item)
        {
            return item.Pattern.ToLower()
                .Replace("{location}", "ho-chi-minh")
                .Replace("{category}", "nha-hang")
                .Replace("{categoryname}", "nha-hang")
                .Replace("{diningname}", "buoi-trua")
                .Replace("{keyword}", "pizza")
                .Replace("{dishcategory}", "ga-ran")
                .Replace("{cuisine}", "viet-nam")
                .Replace("{property}", "wifi")
                .Replace("{purpose}", "an-vat")
                .Replace("{dining}", "buoi-trua")
                .Replace("{area}", "ho-con-rua")
                .Replace("{areadistrict}", "khu-vuc-ho-con-rua")
                .Replace("{districtname}", "quan-1")
                .Replace("&", "&amp;");

        }

        public int Order
        {
            get; set;
        }

        public bool Disabled
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public UrlConfigCollection Urls
        {
            get; set;
        }

        public TestUrlConfigCollection TestUrls
        {
            get; set;
        }

        public NameValueConfigCollection RoutingConstraints
        {
            get; set;
        }

        public NameValueConfigCollection RoutingDefaults
        {
            get; set;
        }
        public NamespaceCollection Namespaces
        {
            get; set;
        }
    }

    public class UrlConfigCollection : List<UrlConfig>
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<Urls>");
            foreach (UrlConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</Urls>");
            return result.ToString();
        }

    }

    public class UrlConfig
    {
        public override string ToString()
        {
            return "<add Pattern=\"" + Pattern + "\" RoutingSchema=\"" + RoutingSchema + "\"></add>";
        }
        public string Pattern
        {
            get; set;
        }

        public RouteSchema RoutingSchema
        {
            get; set;

        }
    }

    public class RoutingDefaultCollection : List<NameValueConfig>
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<RoutingDefaults>");
            foreach (NameValueConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</RoutingDefaults>");
            return result.ToString();
        }

    }

    public class RoutingContraintCollection : List<NameValueConfig>
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<RoutingConstraints>");
            foreach (NameValueConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</RoutingConstraints>");
            return result.ToString();
        }
    }

    public class NameValueConfigCollection : List<NameValueConfig>
    {
        public string ToString(string tagName)
        {
            StringBuilder result = new StringBuilder("<" + tagName + ">");
            foreach (NameValueConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</" + tagName + ">");
            return result.ToString();
        }

    }

    public class NameValueConfig
    {
        public override string ToString()
        {
            return "<add Name=\"" + Name + "\" Value=\"" + Value + "\"></add>";
        }

        public string Name
        {
            get; set;
        }

        public string Value
        {
            get; set;
        }
    }

    public class NamespaceCollection : List<NameConfig>
    {
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("<Namespaces>");
            foreach (NameConfig item in this)
            {
                result.AppendLine(item.ToString());
            }
            result.Append("</Namespaces>");
            return result.ToString();
        }

    }

    public class NameConfig
    {
        public override string ToString()
        {
            return "<add Name=\"" + Name + "\"></add>";
        }
        public string Name
        {
            get; set;
        }
    }
}
