﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Momo.Common.Logging
{
    public class ActionTimeLog
    {
        public ActionTimeLog()
        {
            LogTime = DateTime.Now;
        }
        public ActionTimeLogType ActionType { get; set; }
        public DateTime LogTime { get; set; }
        public TimeSpan Elapsed { get; set; }
        public string ActionInfo { get; set; }
    }

    public enum ActionTimeLogType
    {
        ExeAction = 1,
        ExeCache = 2,
        ExeClearCache = 3,
        ExeDb = 4,
        Exe = 5,
        ExeResult = 6,
    }
}
