﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Momo.Common
{
    public class InjectionProtector<TModel>
    {
        private IList<TModel> DataSource;
        private List<Expression<Func<TModel, string>>> Expressions = new List<Expression<Func<TModel, string>>>();
        public InjectionProtector(TModel model)
        {
            DataSource = new List<TModel>() { model };
        }
        public InjectionProtector(IList<TModel> source)
        {
            DataSource = source;
        }
        public InjectionProtector<TModel> RemoveInjection(Expression<Func<TModel, string>> expression)
        {
            Expressions.Add(expression);
            return this;
        }

        public void Execute()
        {
            if (DataSource != null)
            {
                var propertyName = string.Empty;
                var propertyValue = string.Empty;

                foreach (var model in DataSource)
                {
                    foreach (var expression in Expressions)
                    {
                        propertyName = ((MemberExpression)expression.Body).Member.Name;
                        propertyValue = expression.Compile()(model);
                        if (!string.IsNullOrEmpty(propertyName) && !string.IsNullOrEmpty(propertyValue))
                        {
                            model.GetType().GetProperty(propertyName).SetValue(model, propertyValue.RemoveHTMLInhection().RemoveSQLInjection());
                        }
                    }
                }
            }
        }
    }
}
