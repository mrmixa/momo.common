﻿using Microsoft.AspNetCore.Mvc.Formatters;
using ProtoBuf.Meta;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Momo.Common.Formaters
{
    public class ProtoBufFormatter : Microsoft.AspNetCore.Mvc.Formatters.IOutputFormatter, IInputFormatter
    {
        public ProtoBufFormatter()
        {

        }

        private static Lazy<RuntimeTypeModel> model = new Lazy<RuntimeTypeModel>(CreateTypeModel);

        public static RuntimeTypeModel Model
        {
            get { return model.Value; }
        }


        private static RuntimeTypeModel CreateTypeModel()
        {
            var typeModel = TypeModel.Create();
            typeModel.UseImplicitZeroDefaults = false;
            return typeModel;
        }

        private static bool CanReadTypeCore(Type type)
        {
            return true;
        }

        public bool CanWriteResult(OutputFormatterCanWriteContext context)
        {
            
            return (context.HttpContext.Request.Headers["Accept"].Contains("application/x-protobuf")) == true;
        }

        public Task WriteAsync(OutputFormatterWriteContext context)
        {
            var tcs = new TaskCompletionSource<object>();

            try
            {
                context.HttpContext.Response.ContentType = "application/x-protobuf";
                Model.Serialize(context.HttpContext.Response.Body, context.Object);
                tcs.SetResult(null);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }

            return tcs.Task;
        }

        public bool CanRead(InputFormatterContext context)
        {
            return (context.HttpContext.Request.ContentType?.Contains("application/x-protobuf")) == true;
        }

        public Task<InputFormatterResult> ReadAsync(InputFormatterContext context)
        {
            try
            {
                return InputFormatterResult.SuccessAsync(Model.Deserialize(context.HttpContext.Request.Body, null, context.ModelType));
            }
            catch (Exception ex)
            {
                return InputFormatterResult.FailureAsync();
            }

        }
    }
}