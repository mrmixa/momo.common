﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Momo.Common
{
    public static class Extensions
    {
        public static bool IsNullOrDefault<T>(this T? value) where T : struct
        {
            return default(T).Equals(value.GetValueOrDefault());
        }

        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            return source.PickRandom(1).Single();
        }

        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
        {
            return source.Shuffle().Take(count);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }

        public static JObject ToJson<TKey, TValue>(this Dictionary<TKey, TValue> source)
        {
            var props = new Dictionary<string, JProperty>();
            foreach (var item in source)
	        {
                var key = item.Key.ToString();
                if(props.ContainsKey(key))
                {
                    props[key] = new JProperty(key, item.Value.ToString());
                }
                else
                {
                    props.Add(key, new JProperty(key, item.Value.ToString()));
                }
	        }

            return new JObject(props.Values.ToArray());
        }

        public static string RemoveSQLInjection(this string source)
        {
            return source.Replace("'", "''");
        }

        public static string RemoveHTMLInhection(this string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

    }
}
