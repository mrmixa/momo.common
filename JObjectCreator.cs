﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Momo.Common
{
    public class JObjectCreator<TModel>
    {
        private List<Expression<Func<TModel, object>>> Expressions = new List<Expression<Func<TModel, object>>>();
        TModel Model;

        public JObjectCreator(TModel model)
        {
            Model = model;
        }

        public JObjectCreator<TModel> AddPropertyToJObject(Expression<Func<TModel, object>> expression)
        {
            Expressions.Add(expression);
            return this;
        }

        public JObject CreateJObject(Dictionary<string, object> extendProperties = null)
        {
            try
            {
                var jObject = new JObject();
                string propertyName;
                object propertyValue;

                foreach (var expression in Expressions)
                {
                    if (expression.Body is MemberExpression)
                    {
                        propertyName = ((MemberExpression)expression.Body).Member.Name;
                    }
                    else
                    {
                        propertyName = ((MemberExpression)(((UnaryExpression)expression.Body).Operand)).Member.Name;
                    }

                    propertyValue = expression.Compile()(Model);
                    if (!string.IsNullOrEmpty(propertyName) && propertyValue != null)
                    {
                        jObject.Add(propertyName, JToken.FromObject(propertyValue));
                    }
                }

                if (extendProperties != null)
                {
                    foreach (var prop in extendProperties)
                    {
                        jObject.Add(prop.Key, JToken.FromObject(prop.Value));
                    }
                }

                return jObject;
            }
            finally
            {
                Expressions.Clear();
            }
        }
    }
}
